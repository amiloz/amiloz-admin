import { useMutation } from 'react-query'
import axios from 'axios'
import { toast } from 'react-toastify'

export const useUploadDocument = (onSuccess, typeOfFile) => {
  return useMutation(
    async (selectedFile) => {
      const formData = new FormData()
      formData.append('image', selectedFile)
      formData.append('type', typeOfFile)
      // toast('Uploading ' + selectedFile.name)
      return axios({
        method: 'POST',
        url: `/upload`,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        data: formData,
      })
    },
    {
      onSuccess: (res, selectedFile) => {
        // toast('Sucessfully uploaded ' + selectedFile.name)
        onSuccess(res.data.file_url, selectedFile)
      },
      onError: (error) => {
        if (error.response.data.code.toString()[0] === '4') {
          toast(error.response.data.message)
        }
      },
    }
  )
}
