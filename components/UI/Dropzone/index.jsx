/* eslint-disable @next/next/no-img-element */
import { useEffect, useRef, useState } from 'react'
import { Row, Col } from 'react-bootstrap'
import { toast } from 'react-toastify'
import { useUploadDocument } from '../../../hooks/api-calls/general/uploadDocuments'
import styles from './index.module.css'

export const imageTypes = [
  'image/jpeg',
  'image/jpg',
  'image/png',
  'image/gif',
  'image/x-icon',
]
export const videoTypes = ['video/mp4']
export const docsTypes = ['application/pdf']

const Dropzone = (props) => {
  const {
    height,
    width,
    firstText,
    textToHighlight,
    lastText,
    greyText,
    smallTextArea, // to give small text area to inner msg
    showUploadIcon, // hides the dot and instead shows upload icon
    onSuccessImgSelectedFn, // executed when a image is selected
    supportedTypes, // can be images or videos or documents (send array of types)
    isMultiSelect, // boolean(enable to allow multiple selection)
    previewWidth, //optional
    previewHeight, // optional
    maxFileSize, // in MBs
    documents, // documents to preview
    removeItem, // to remove preview item(optional)
    hideDropzoneText, // doesn't show any text in the middle(optional)
    preConditionFn, // checks if a condition is matched before uploading, return T/F(optional)
    typeOfUploading, // checks what type of image will be uploaded [store,product, brand , category]
  } = props

  const [selectedFiles, setSelectedFiles] = useState([])
  const fileInputRef = useRef()

  const { isLoading: uploading, mutate: upload } = useUploadDocument(
    onSuccessImgSelectedFn, typeOfUploading
  )

  // validate each file
  const validateFile = (file) => {
    const validTypes = []
    if (supportedTypes.includes('images')) {
      validTypes.push(...imageTypes)
    }
    if (supportedTypes.includes('videos')) {
      validTypes.push(...videoTypes)
    }
    if (supportedTypes.includes('documents')) {
      validTypes.push(...docsTypes)
    }
    if (validTypes.indexOf(file.type) === -1) {
      return false
    }
    return true
  }
  const handleFiles = (files) => {
    if (!isMultiSelect && files.length >= 2) {
      toast('Please select single file only')
      return
    }
    for (let i = 0; i < files.length; i++) {
      if (maxFileSize && files[i].size > maxFileSize * 1024 * 1024) {
        toast('File size greater than ' + maxFileSize + 'MB')
      } else {
        if (validateFile(files[i])) {
          // add to an array so we can display the name of file
          if (isMultiSelect) {
            setSelectedFiles((prevArray) => [...prevArray, files[i]])
          } else {
            setSelectedFiles([files[i]])
          }
        } else {
          toast(`${files[i].name} has invalid file type`)
        }
      }
    }
  }
  const dragOver = (e) => {
    e.preventDefault()
  }
  const dragEnter = (e) => {
    e.preventDefault()
  }
  const dragLeave = (e) => {
    e.preventDefault()
  }
  const fileDrop = (e) => {
    e.preventDefault()
    const result = preConditionFn ? preConditionFn() : true
    if (result === false) {
      return
    }

    const files = e.dataTransfer.files
    console.log(files)
    if (files.length) {
      handleFiles(files)
    }
  }
  const fileInputClicked = () => {
    const result = preConditionFn ? preConditionFn() : true
    if (result === false) {
      return
    }
    fileInputRef.current.click()
  }
  const filesSelected = () => {
    if (fileInputRef.current.files.length) {
      handleFiles(fileInputRef.current.files)
    }
  }

  useEffect(() => {
    if (selectedFiles.length >= 1) {
      selectedFiles.forEach(async (file) => {
        upload(file)
      })
      setSelectedFiles([])
    }
  }, [selectedFiles, upload])
  // console.log('d', documents.length)
  return (
    <>
      <div
        className={`${
          documents?.length === 1 &&
          !isMultiSelect &&
          supportedTypes.includes('images')
            ? ''
            : styles.dropContainer
        }`}
        style={{ height: height, width: width }}
        onDragOver={dragOver}
        onDragEnter={dragEnter}
        onDragLeave={dragLeave}
        onDrop={fileDrop}
      >
        {documents?.length === 1 &&
        !isMultiSelect &&
        supportedTypes.includes('images') ? (
          <div onClick={fileInputClicked} className="cursor-pointer">
            <Preview
              type={documents[0].type}
              link={documents[0].link}
              height={height}
              width={width}
            />
          </div>
        ) : (
          <div
            className={`${
              hideDropzoneText ? styles.EditIcon : styles.dropMessage
            } ${showUploadIcon ? styles.showUploadIcon : ''}`}
          >
            {hideDropzoneText ? (
              <>
                <img
                  src="/icons/edit_dark.svg"
                  alt="edit-icon"
                  className="cursor-pointer"
                  onClick={fileInputClicked}
                />
              </>
            ) : (
              <>
                {!showUploadIcon && <span className={styles.Dot} />}
                <div
                  className={`${styles.Text} ${
                    smallTextArea ? styles.LargePadding : ''
                  }`}
                >
                  {showUploadIcon && (
                    <>
                      <img src="/images/upload.png" alt="upload-icon" />
                      &nbsp;&nbsp;
                    </>
                  )}
                  {firstText}{' '}
                  <span
                    className={styles.HighlightedText}
                    onClick={fileInputClicked}
                  >
                    {textToHighlight}
                  </span>{' '}
                  {lastText}
                </div>
                <div className={styles.GreyText}>{greyText}</div>
              </>
            )}
          </div>
        )}
        <input
          ref={fileInputRef}
          className={styles.fileInput}
          type="file"
          multiple={isMultiSelect ? true : false}
          onChange={filesSelected}
        />
      </div>
      {uploading && <p>Uploading...</p>}

      {/* show preview if preview height and width is set and type is multi-select */}
      {isMultiSelect && previewWidth && previewHeight && documents?.length ? (
        <>
          <div className={styles.PreviewContainer}>
            <p className={styles.NoOfFilesSelected}>
              {documents.length} files selected
            </p>
            <Row className="m-0">
              {documents.map((selectedFile, idx) => (
                <Preview
                  key={idx}
                  link={selectedFile.link}
                  width={previewWidth}
                  height={previewHeight}
                  type={selectedFile.type}
                  removeItem={removeItem}
                />
              ))}
            </Row>
          </div>
        </>
      ) : null}
    </>
  )
}
export default Dropzone

export const Preview = (props) => {
  const { type, height, width, link, removeItem } = props
  let jsx = <></>
  if (imageTypes.includes(type)) {
    jsx = (
      <img
        alt="dropzone-img"
        src={link}
        width={width}
        height={height}
        style={{ maxWidth: '100%' }}
        className={styles.PreviewStyles}
      />
    )
  } else if (videoTypes.includes(type)) {
    jsx = (
      <video width={width} height={height} controls>
        <source src={link} type="video/mp4" />
      </video>
    )
  }
  return (
    <Col
      xs="auto"
      className="p-0"
      style={{
        height: height,
        width: width,
        marginRight: '12px',
        marginBottom: '12px',
        position: 'relative',
      }}
    >
      {removeItem && (
        <img
        src="/images/close.png" alt="X"
        className="cursor-pointer"
          onClick={() => removeItem(link)}
        />
      )}
      {jsx}
    </Col>
  )
}
