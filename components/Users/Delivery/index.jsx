import axios from 'axios'
import moment from 'moment'
import { useEffect, useRef, useState } from 'react'
import { Col, Container, Image, Row } from 'react-bootstrap'
import styles from '../index.module.css'
import UserCreateModal from './UserCreateModal'
import DropdownTask from './DropdownTask'
import Pagination from 'react-js-pagination'

const Delivery = () => {
  const [showModal, setShowModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [users, setUsers] = useState([])
  const [selectedName, setSelectedName] = useState({})
  const [windowWidth, setWindowWidth] = useState(window.innerWidth)
  const [sortDir, setSortDir] = useState(false)
  const [displayFilterMenu, setDisplayFilterMenu] = useState(false)
  const [displaySortMenu, setDisplaySortMenu] = useState(false)
  const [filterKey, setFilterKey] = useState(null)
  const [sortKey, setSortKey] = useState(null)
  const [userData, setUserData] = useState(null)
  const [activePage, setActivePage] = useState(1)
  const [total, setTotal] = useState(0)

  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplayFilterMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const wrapperRef = useRef(null)
  useOutsideAlerter(wrapperRef)
  function useOutsideAlerterSort(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplaySortMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const sortRef = useRef(null)
  useOutsideAlerterSort(sortRef)

  const handleResize = (e) => {
    setWindowWidth(window.innerWidth)
  }
  useEffect(() => {
    window.addEventListener('resize', handleResize)

    return () => {
      window.removeEventListener('resize', handleResize)
    }
  }, [])

  const getAllIndusries = (
    query,
    page = 1,
    filterKeyValue,
    sortKeyValue,
    sortDirection
  ) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/users/all?role=delivery_rep`
    if (query) {
      url += `&name=${encodeURIComponent(query)}`
    }
    if (filterKeyValue) {
      url += `&is_active=${filterKeyValue}`
    }
    if (sortKeyValue) {
      const dir = sortDirection === true ? 'desc' : 'asc'
      url += `&sort_by=${sortKeyValue}&sort_dir=${dir}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setUsers(res.data.users)
      setTotal(res.data.total)
    })
  }
  useEffect(() => {
    getAllIndusries()
  }, [])

  const filterKeyChangeHandler = (e) => {
    const { value } = e.target
    if (e.target.checked) {
      setFilterKey(value)
      getAllIndusries(null, 1, value)
    } else {
      setFilterKey(null)
      getAllIndusries(null, 1, null)
    }
    setDisplayFilterMenu(false)
  }
  const sortKeyChangeHandler = (dir, e) => {
    const { value } = e.target
    if (e.target.checked) {
      setSortKey(value)
      setSortDir(dir)
      getAllIndusries(null, 1, filterKey, value, dir)
    } else {
      setSortKey(null)
      getAllIndusries(null, 1, filterKey, null)
    }
    setDisplaySortMenu(false)
  }
  return (
    <>
      <Row>
        <Col>
          <div style={{ position: 'relative' }}>
            <input
              className={styles.searchInput}
              placeholder="Buscar Usuarios"
              onChange={(e) => getAllIndusries(e.target.value)}
            />
            <Image
              src="/images/searchIcon.png"
              alt=""
              height="16px"
              width="16px"
              className={styles.searchInputIcon}
            />
          </div>
        </Col>
        <Col>
          <div className={styles.filterSortDiv}>
            <button
              type="button"
              onClick={() => {
                setShowModal(true)
                setUserData({ modalType: 'ADD', industry: '' })
              }}
              className={styles.buttonAdd}
            >
              + Agregar Usuario
            </button>
            {/* <div className={styles.filterByDiv}>
              <span onClick={() => setDisplayFilterMenu(!displayFilterMenu)}>
                Filter by{' '}
                <span style={{ float: 'right' }}>
                  <i className="fa fa-caret-down" aria-hidden="true"></i>
                </span>
              </span>
              {displayFilterMenu ? (
                <ul ref={wrapperRef}>
                  <li className={filterKey === 'true' && styles.filterByDivLi}>
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <label className={styles.containerCheckbox}>
                          <input
                            type="checkbox"
                            value="true"
                            checked={filterKey === 'true'}
                            onChange={(e) => filterKeyChangeHandler(e)}
                          />
                          <span className={styles.checkmark}></span>
                        </label>
                      </span>
                      Active
                    </label>
                  </li>
                  <li className={filterKey === 'false' && styles.filterByDivLi}>
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <label className={styles.containerCheckbox}>
                          <input
                            type="checkbox"
                            value="false"
                            checked={filterKey === 'false'}
                            onChange={(e) => filterKeyChangeHandler(e)}
                          />
                          <span className={styles.checkmark}></span>
                        </label>
                      </span>
                      Inactive
                    </label>
                  </li>
                </ul>
              ) : null}
            </div> */}
            <div className={styles.sortByDiv}>
              <span onClick={() => setDisplaySortMenu(!displaySortMenu)}>
                Ordenar por{' '}
                <span style={{ float: 'right' }}>
                  <i className="fa fa-caret-down" aria-hidden="true"></i>
                </span>
              </span>
              {displaySortMenu ? (
                <ul ref={sortRef}>
                  <li
                    className={
                      sortKey === 'name' && !sortDir && styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="name"
                          checked={sortKey === 'name' && !sortDir}
                          onChange={(e) => sortKeyChangeHandler(false, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Name (Ascending)
                    </label>
                  </li>
                  <li
                    className={
                      sortKey === 'name' && sortDir && styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="name"
                          checked={sortKey === 'name' && sortDir}
                          onChange={(e) => sortKeyChangeHandler(true, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Name (Descending)
                    </label>
                  </li>
                </ul>
              ) : null}
            </div>
          </div>
        </Col>
      </Row>
      {windowWidth > 800 ? (
        <>
          <table className={styles.myTable} style={{ marginBottom: '5px' }}>
            <thead>
              <tr>
                <th className={styles.theadTh1} id="name">
                  Nombre
                </th>
                <th className={styles.theadTh}>Contacto</th>
                <th className={styles.theadTh1}>Género</th>
                <th className={styles.theadTh1}></th>
              </tr>
            </thead>
            <tbody>
              {users?.map((item) => {
                const { id, name, email, mobile, gender } = item
                return (
                  <tr
                    key={id}
                    className={styles.tbodyTr}
                    style={{ margin: '12px 0' }}
                  >
                    <td id="name" className={styles.tbodyTd1}>
                      {name}
                    </td>
                    <td className={styles.tbodyTd}>
                      <div className={styles.tbodyTdDiv}>
                        <span className={styles.tbodyTdSpan}>Email</span>
                        {email}
                      </div>

                      <br />
                      <div className={styles.tbodyTdDiv}>
                        <span className={styles.tbodyTdSpan}>Móvil</span>
                        {mobile}
                      </div>
                    </td>
                    <td className={styles.tbodyTd1}>{gender}</td>
                    <td className={styles.tbodyTd1}>
                      <button
                        type="button"
                        onClick={() => {
                          setUserData({ modalType: 'EDIT', industry: item })
                          setShowModal(true)
                        }}
                        className={styles.buttonEditProd}
                      >
                        Editar Usuarios
                      </button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </>
      ) : (
        <>
          <div>
            {users?.map((item) => {
              const { id, name, email, mobile, gender } = item
              return (
                <div key={id} className={styles.tableMobDiv}>
                  <div className={styles.tableMobDivFlex}>
                    <span className={styles.tableMobDivFlex1}>{name}</span>
                    <span className={styles.tableMobDivFlex2}>
                      <DropdownTask
                        setShowModal={setShowModal}
                        setUserData={setUserData}
                        industry={item}
                      />
                    </span>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Email:</span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>{email}</div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Móvil: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>{mobile}</div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Género: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>{gender}</div>
                  </div>
                </div>
              )
            })}
          </div>
        </>
      )}
      <UserCreateModal
        show={showModal}
        onHide={() => setShowModal(false)}
        handleClose={() => setShowModal(false)}
        getAllIndusries={getAllIndusries}
        userData={userData}
      />
      <div
        style={{
          marginTop: '20px',
          borderTop: '1px solid #e7e7e7',
          textAlign: 'right',
          paddingTop: '20px',
          minHeight: '62px',
        }}
      >
        <div style={{ float: 'right' }}>
          <Pagination
            activePage={activePage}
            itemsCountPerPage={30}
            totalItemsCount={total}
            pageRangeDisplayed={5}
            onChange={(page) => {
              setActivePage(page)
              getAllIndusries(null, page)
            }}
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>
      </div>
    </>
  )
}

export default Delivery
