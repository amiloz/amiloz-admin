import axios from 'axios'
import { useState, useEffect } from 'react'
import { Form, Image, Modal } from 'react-bootstrap'
import styles from '../index.module.css'
import Dropzone from '../../UI/Dropzone'

const UserCreateModal = ({ show, handleClose, getAllIndusries, userData }) => {
  const [name, setName] = useState('')
  const [mobile, setMobile] = useState('')
  const [email, setEmail] = useState('')
  const [gender, setGender] = useState('')
  const [image, setImage] = useState([])
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    if (show) setName(userData?.industry.name || '')
    setMobile(userData?.industry.mobile || '')
    setEmail(userData?.industry.email || '')
    setGender(userData?.industry.gender || '')
  }, [show])

  const addItem = (e) => {
    e.preventDefault()
    setLoading(true)
    axios({
      method: 'POST',
      url: `/users`,
      data: {
        name,
        mobile,
        email,
        gender,
        role: 'sales_rep',
      },
    })
      .then((res) => {
        closeStateAndClearModal()
        getAllIndusries()
        setName('')
        setEmail('')
        setMobile('')
        setGender('')
        setLoading(false)
      })
      .catch((err) => {
        setLoading(false)
      })
  }
  const editItem = (e) => {
    e.preventDefault()
    setLoading(true)
    axios({
      method: 'PUT',
      url: `/users/${userData.industry.id}`,
      data: {
        name,
        mobile: String(mobile),
        gender,
        role: 'sales_rep',
      },
    })
      .then((res) => {
        closeStateAndClearModal()
        getAllIndusries()
        setLoading(false)
      })
      .catch((err) => {
        setLoading(false)
      })
  }
  const submitHandler = (e) => {
    if (userData.modalType === 'ADD') {
      addItem(e)
    } else editItem(e)
  }
  const closeStateAndClearModal = () => {
    handleClose()
    setName('')
    setMobile('')
    setImage([])
  }
  console.log('img', image)
  return (
    <Modal
      show={show}
      onHide={closeStateAndClearModal}
      fullscreen="md-down"
      backdrop="static"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div
          className={styles.mobileModalDiv}
          onClick={() => closeStateAndClearModal()}
        >
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            {userData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Usuario
          </p>
        </div>
        <button
          type="button"
          onClick={() => closeStateAndClearModal()}
          className={styles.modalCloseButton}
        >
          <Image src="/images/close.png" alt="X" />
        </button>
        <h3 className={styles.modalHeadingMain}>
          {' '}
          {userData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Usuario
        </h3>
        <Form onSubmit={(e) => submitHandler(e)}>
          <Form.Label className={styles.modalLabel}>Nombre</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter User name"
            onChange={(e) => setName(e.target.value)}
            value={name}
            required
            className={styles.modalInput}
          />
          <Form.Label className={styles.modalLabel}>Móvil</Form.Label>
          <Form.Control
            placeholder="Enter Mobile"
            onChange={(e) => setMobile(e.target.value)}
            value={mobile}
            required
            className={styles.modalInput}
            type="text"
          />
          <Form.Label className={styles.modalLabel}>Email</Form.Label>
          <Form.Control
            placeholder="Enter Email"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            required
            className={styles.modalInput}
          />
          <Form.Label className={styles.modalLabel}>Género</Form.Label>
          <select
            className={styles.modalInput}
            style={{ width: '100%', display: 'block' }}
            value={gender}
            onChange={(e) => setGender(e.target.value)}
          >
            <option hidden>-Seleccione-</option>
            <option disabled selected>
              -Seleccione-
            </option>

            <option value="male">Masculino</option>
            <option value="female">Mujer</option>
          </select>
          <div className={styles.modalButtonDiv}>
            <button
              type="submit"
              disabled={loading || name === ''}
              className={
                loading || name === ''
                  ? styles.modalDisabledButton
                  : styles.modalSaveButton
              }
            >
              {userData?.modalType === 'ADD' ? 'Agregar' : 'Editar'}
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default UserCreateModal
