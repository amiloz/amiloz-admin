import { useRouter } from 'next/dist/client/router'
import { useEffect, useState } from 'react'
import styles from './index.module.css'
import Delivery from './Delivery'
import Seller from './Seller'
import withAuth from '../../hoc/withAuth'

const UsersRoute = () => {
  const router = useRouter()
  const [tab, setTab] = useState('')

  useEffect(() => {
    setTab(router.query.tab)
  }, [router.query.tab])

  let content = ''
  if (tab === 'delivery-rep') {
    content = <Delivery />
  } else if (tab === 'sales-rep') {
    content = <Seller />
  }

  return <div className={styles.Container}>{content}</div>
}

export default withAuth(UsersRoute)
