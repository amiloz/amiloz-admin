import { Col, Image, Modal, Row } from 'react-bootstrap'
import styles from '../index.module.css'

const ViewSummaryModal = ({
  show,
  handleClose,
  productsSummary,
  settleOrder,
  settleLoader,
  selectedOrdersData,
}) => {
  const showButton = selectedOrdersData?.every(
    (data) => data.status === 'Delivered' && data.is_settled === 0
  )
  return (
    <Modal
      show={show}
      onHide={handleClose}
      fullscreen="md-down"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
      size="lg"
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div className={styles.mobileModalDiv} onClick={() => handleClose()}>
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            Resumen del pedido
          </p>
        </div>
        <button
          type="button"
          onClick={() => handleClose()}
          className={styles.modalCloseButton}
        >
          <Image src="/images/close.png" alt="X" />
        </button>
        <h3 className={styles.modalHeadingMain}>Resumen del pedido</h3>
        <Row>
          <Col lg={2} className={styles.orderSummaryHeader}>
            Imagen principal
          </Col>
          <Col lg={5} className={styles.orderSummaryHeader}>
            Nombre
          </Col>
          <Col lg={3} className={styles.orderSummaryHeader}>
            Especificaciones
          </Col>
          <Col lg={2} className={styles.orderSummaryHeader}>
            <div style={{ float: 'right' }}>Leftovers</div>
          </Col>
        </Row>
        {productsSummary?.map((prod) => (
          <div className={styles.summaryDiv} key={prod.product_id}>
            <Row>
              <Col lg={2}>
                <Image
                  src={prod.primary_image}
                  alt=""
                  width="50px"
                  height="50px"
                />
              </Col>
              <Col lg={5} style={{ paddingTop: '12px' }}>
                <h2 className={styles.summaryHeading}>{prod.product_name}</h2>
              </Col>
              <Col lg={3} style={{ paddingTop: '12px' }}>
                <p className={styles.summaryDesc}>
                  <span
                    style={{
                      fontSize: '12px',
                      lineHeight: '16px',
                      color: '#0F1111',
                      fontWeight: '500',
                    }}
                  >
                    Cantidad:
                  </span>{' '}
                  {prod.quantity}
                  <br />
                  <span
                    style={{
                      fontSize: '12px',
                      lineHeight: '16px',
                      color: '#0F1111',
                      fontWeight: '500',
                    }}
                  >
                    Peso:
                  </span>{' '}
                  {prod.weight} {prod.weight_unit}
                  <br />
                  <span
                    style={{
                      fontSize: '12px',
                      lineHeight: '16px',
                      color: '#0F1111',
                      fontWeight: '500',
                    }}
                  >
                    Unidad:
                  </span>{' '}
                  {prod.unit_in_package}
                </p>
              </Col>
              <Col lg={2} style={{ paddingTop: '12px' }}>
                <p className={styles.summaryDesc} style={{ float: 'right' }}>
                  {prod.left_quantity}
                </p>
              </Col>
            </Row>
          </div>
        ))}
        {showButton && (
          <button
            onClick={() => settleOrder()}
            type="button"
            className={styles.myCartButton}
            disabled={settleLoader}
            style={settleLoader ? { background: '#e7e7e7' } : {}}
          >
            Mark Entregado
          </button>
        )}
      </Modal.Body>
    </Modal>
  )
}

export default ViewSummaryModal
