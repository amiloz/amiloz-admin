import axios from 'axios'
import { useState } from 'react'
import { Form, Image, Modal } from 'react-bootstrap'
import styles from '../index.module.css'

const AssignDeliveryRepModal = ({
  show,
  handleClose,
  setSelectedDeliveryRep,
  deliveryReps,
  submitHandler,
  loading,
}) => {
  return (
    <Modal
      show={show}
      onHide={handleClose}
      fullscreen="md-down"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div className={styles.mobileModalDiv} onClick={() => handleClose()}>
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            Assign Delivery Rep
          </p>
        </div>
        <button
          type="button"
          onClick={() => handleClose()}
          className={styles.modalCloseButton}
        >
          <Image src="/images/close.png" alt="X" />
        </button>
        <h3 className={styles.modalHeadingMain}>Assign Delivery Rep</h3>
        <Form onSubmit={(e) => submitHandler(e)}>
          <div>
            <select
              onChange={(e) => setSelectedDeliveryRep(e.target.value)}
              className={styles.modalInput}
              style={{ width: '90%' }}
            >
              <option hidden>-select-</option>
              <option disabled selected>
                -select-
              </option>
              {deliveryReps?.map((rep) => (
                <option key={rep.id} value={rep.id}>
                  {rep.name}
                </option>
              ))}
            </select>
          </div>
          <div className={styles.modalButtonDiv}>
            {/* <button
              type="button"
              onClick={handleClose}
              className={styles.modalCancelButton}
            >
              Cancel
            </button> */}
            <button
              type="submit"
              className={
                loading ? styles.modalDisabledButton : styles.modalSaveButton
              }
              disabled={loading}
            >
              Assign
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default AssignDeliveryRepModal
