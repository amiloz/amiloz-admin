import axios from 'axios'
import { useEffect, useState } from 'react'
import { Col, Form, Image, Modal, Row } from 'react-bootstrap'
import Select from 'react-select'
import { toast } from 'react-toastify'
import styles from '../index.module.css'

//multi select design
const colourStyles1 = {
  control: (styles, { data, isDisabled, isFocused, isSelected }) => ({
    ...styles,
    padding: '3px 8px',
    background: '#fff',
    border: isFocused
      ? '1px solid #BACCDD'
      : isSelected
      ? '1px solid #BACCDD'
      : '1px solid #BACCDD',
    '&:hover': {
      borderColor: '#BACCDD',
    },
    boxSizing: 'border-box',
    borderRadius: '4px',
    outline: 'none',
    fontSize: '14px',
    boxShadow: isFocused ? 'none' : isSelected ? 'none' : 'none',
  }),
  menuPortal: () => {
    return {
      backgroundColor: '#E0F7FA',
    }
  },
  singleValue: (styles) => {
    return {
      ...styles,
      color: '#2D4C66',
      fontSize: '14px',
      lineHeight: '20px',
      letterpacing: '0.25px',
    }
  },
  indicatorSeparator: (styles) => {
    return {
      display: 'none',
    }
  },
  placeholder: (styles) => {
    return {
      ...styles,
      color: '#BACCDD',
      fontSize: '14px',
      lineHeight: '20px',
      letterSpacing: '0.25px',
    }
  },
}

const StoreChangeModal = ({
  show,
  handleClose,
  getAllStores,
  selectedIndustry,
  setStoreId,
  stores,
  getAllIndusries,
  storeId,
  findStore,
  storeData,
  submitHandler,
  orderDate,
}) => {
  const [loading, setLoading] = useState(false)
  const [selectedStore, setSelectedStore] = useState('')
  const [storeType, setStoreType] = useState('edit')
  const [selectedStoreData, setSelectedStoreData] = useState({
    name: '',
    mobile: '',
    poc_name: '',
    poc_mobile: '',
    street: '',
    city: '',
    state: '',
    country: '',
    postal_code: '',
    description: '',
  })
  const multiselectLocationChangeHandler = (selected) => {
    setSelectedStore(selected.value)
    const myStore = stores.find(
      (store) => store.id === parseInt(selected.value)
    )
    setSelectedStoreData(myStore)
    setStoreType('edit')
  }
  const selectedStoreDataChangeHandler = (e) => {
    const { name, value } = e.target
    const storeData = { ...selectedStoreData, [name]: value }
    setSelectedStoreData(storeData)
  }
  const storeDataSave = (storeId) => {
    const {
      name,
      mobile,
      poc_name,
      poc_mobile,
      street,
      city,
      state,
      country,
      description,
      postal_code,
    } = selectedStoreData
    setLoading(true)
    axios({
      url: `/stores/${storeId}`,
      method: `PUT`,
      data: {
        name,
        mobile: mobile.toString(),
        poc_name,
        poc_mobile: poc_mobile.toString(),
        street,
        city,
        state,
        country,
        description,
        postal_code: postal_code.toString(),
      },
    })
      .then((res) => {
        getAllStores()
        setStoreId(storeId)
        handleClose()
        findStore()
        setLoading(false)
        submitHandler(orderDate, storeId)
      })
      .catch((err) => setLoading(false))
  }
  const storeDataNew = () => {
    const {
      name,
      mobile,
      poc_name,
      poc_mobile,
      street,
      city,
      state,
      country,
      description,
      postal_code,
    } = selectedStoreData
    setLoading(true)
    axios({
      url: `/stores`,
      method: `POST`,
      data: {
        name,
        mobile: mobile.toString(),
        poc_name,
        poc_mobile: poc_mobile.toString(),
        street,
        city,
        state,
        country,
        description,
        postal_code: postal_code.toString(),
      },
    })
      .then((res) => {
        toast('Store Saved successfully')
        findStore()
        getAllStores()
        setSelectedStore(res.data.store.id)
        setStoreId(res.data.store.id)
        handleClose()
        setLoading(false)
        submitHandler(orderDate, res.data.store.id)
      })
      .catch((err) => setLoading(false))
  }
  return (
    <Modal
      show={show}
      onHide={handleClose}
      fullscreen="md-down"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
      size="lg"
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div className={styles.mobileModalDiv} onClick={() => handleClose()}>
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            Change Store
          </p>
        </div>
        <h3 className={styles.modalHeadingMain}>Change Store</h3>
        <Form>
          <div>
            <div style={{ display: 'flex' }}>
              <div style={{ width: '45%' }}>
                <label className={styles.modalLabel}>
                  Select from existing stores
                </label>
                <div>
                  <Select
                    onChange={(selected) =>
                      multiselectLocationChangeHandler(selected)
                    }
                    options={stores?.map((store) => {
                      return { value: store.id, label: store.name }
                    })}
                    styles={colourStyles1}
                    placeholder="Select"
                    isClearable={false}
                    value={
                      selectedStore
                        ? {
                            value: selectedStore,
                            label: selectedStoreData.name,
                          }
                        : ''
                    }
                  />
                </div>
              </div>
              <div style={{ margin: '4% 4% 0 4%' }}>OR</div>
              <div style={{ width: '45%' }}>
                <label className={styles.modalLabel}> </label>
                <div>
                  <button
                    type="button"
                    onClick={() => {
                      setStoreType('add')
                      setSelectedStoreData({
                        name: '',
                        mobile: '',
                        poc_name: '',
                        poc_mobile: '',
                        street: '',
                        city: '',
                        state: '',
                        country: '',
                        postal_code: '',
                        description: '',
                      })
                    }}
                    className={styles.saveChangesButton}
                    style={{ width: '100%', marginBottom: '0' }}
                  >
                    Add new store
                  </button>
                </div>
              </div>
            </div>
            {selectedStoreData !== {} && (
              <div>
                <Row>
                  <Col>
                    <label className={styles.modalLabel}>Store Name</label>
                    <input
                      type="text"
                      placeholder="Enter store name"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.name}
                      required
                      className={styles.modalInput}
                      name="name"
                    />
                  </Col>
                  <Col>
                    <label className={styles.modalLabel}>Store Contact</label>
                    <input
                      type="text"
                      placeholder="Enter store contact number"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.mobile}
                      required
                      className={styles.modalInput}
                      name="mobile"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <label className={styles.modalLabel}>Person Name</label>
                    <input
                      type="text"
                      placeholder="Enter name"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.poc_name}
                      required
                      className={styles.modalInput}
                      name="poc_name"
                    />
                  </Col>
                  <Col>
                    <label className={styles.modalLabel}>Person Contact</label>
                    <input
                      type="text"
                      placeholder="Enter contact number"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.poc_mobile}
                      required
                      className={styles.modalInput}
                      name="poc_mobile"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <label className={styles.modalLabel}>Street</label>
                    <input
                      type="text"
                      placeholder="Enter street"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.street}
                      required
                      className={styles.modalInput}
                      name="street"
                    />
                  </Col>
                  <Col>
                    <label className={styles.modalLabel}>City</label>
                    <input
                      type="text"
                      placeholder="Enter city"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.city}
                      required
                      className={styles.modalInput}
                      name="city"
                    />
                  </Col>
                  <Col>
                    <label className={styles.modalLabel}>State</label>
                    <input
                      type="text"
                      placeholder="Enter state"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.state}
                      required
                      className={styles.modalInput}
                      name="state"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <label className={styles.modalLabel}>Country</label>
                    <input
                      type="text"
                      placeholder="Enter country"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.country}
                      required
                      className={styles.modalInput}
                      name="country"
                    />
                  </Col>
                  <Col>
                    <label className={styles.modalLabel}>Postal Code</label>
                    <input
                      type="text"
                      placeholder="Enter postal code"
                      onChange={(e) => selectedStoreDataChangeHandler(e)}
                      value={selectedStoreData.postal_code}
                      required
                      className={styles.modalInput}
                      name="postal_code"
                    />
                  </Col>
                </Row>
                <label className={styles.modalLabel}>Description</label>
                <textarea
                  placeholder="Enter description"
                  onChange={(e) => selectedStoreDataChangeHandler(e)}
                  value={selectedStoreData.description}
                  required
                  className={styles.modalInput}
                  name="description"
                />
              </div>
            )}
          </div>
          <div className={styles.modalButtonDiv}>
            <button
              type="button"
              onClick={() =>
                storeType === 'edit'
                  ? storeDataSave(selectedStoreData.id)
                  : storeDataNew()
              }
              disabled={loading}
              className={
                loading ? styles.modalDisabledButton : styles.modalSaveButton
              }
            >
              Save Changes
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default StoreChangeModal
