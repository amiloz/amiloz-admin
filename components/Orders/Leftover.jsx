import axios from 'axios'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import styles from './index.module.css'

function Leftover() {
  const [summary, setSummary] = useState([])
  const [date, setDate] = useState(new Date())
  const [deliveryRep, setDeliveryRep] = useState('')
  const [deliveryRepList, setDeliveryRepList] = useState([])
  const getDeliveryReps = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/users/all?role=delivery_rep`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setDeliveryRepList(res.data.users)
    })
  }
  const getSummary = (dateFilter, deliveryFilter) => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'POST',
      url: `/orders/summary`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
      data: {
        date: moment(dateFilter).format('YYYY-MM-DD'),
        ...(deliveryFilter && { delivery_rep_id: deliveryFilter }),
      },
    }).then((res) => {
      setSummary(res.data.products)
    })
  }
  useEffect(() => {
    getSummary()
    getDeliveryReps()
  }, [])
  return (
    <div className={styles.Container} style={{ marginLeft: '0' }}>
      <Row>
        <div style={{ display: 'flex' }}>
          <div>
            <label className={styles.modalLabel}>Filtrar por fecha</label>
            <DatePicker
              selected={date}
              onChange={(date) => {
                setDate(date)
                getSummary(date, deliveryRep)
              }}
              placeholderText="Filtrar por fecha"
              className={styles.modalInputDate}
            />
          </div>
          <div style={{ marginLeft: '20px' }}>
            <label className={styles.modalLabel}>Filtrar por repartidor</label>
            <select
              value={deliveryRep}
              onChange={(e) => {
                setDeliveryRep(e.target.value)
                getSummary(date, e.target.value)
              }}
              className={styles.modalInput}
              style={{ padding: '4.5px 16px' }}
            >
              <option hidden>All</option>
              <option selected value="">
                All
              </option>
              {deliveryRepList?.map((rep) => (
                <option key={rep.id} value={rep.id}>
                  {rep.name}
                </option>
              ))}
            </select>
          </div>
        </div>
      </Row>
      <div className={styles.orderPlaceDiv}>
        <h3 className={styles.detailPageHeadingSub}>Resumen del pedido</h3>
        <div>
          <Row>
            <Col lg={2} className={styles.orderSummaryHeader}>
              Imagen
            </Col>
            <Col lg={5} className={styles.orderSummaryHeader}>
              Nombre
            </Col>
            <Col lg={3} className={styles.orderSummaryHeader}>
              Especificaciones
            </Col>
            <Col lg={2} className={styles.orderSummaryHeader}>
              <div style={{ float: 'right' }}>Producto sin vender</div>
            </Col>
          </Row>
          {summary?.map((prod) =>
            summary.length === 0 ? (
              <span className={styles.OrderName}>
                No hay pedidos para mostrar
              </span>
            ) : (
              <div className={styles.summaryDiv} key={prod.product_id}>
                <Row>
                  <Col lg={2}>
                    <Image
                      src={prod.primary_image}
                      alt=""
                      width="50px"
                      height="50px"
                    />
                  </Col>
                  <Col lg={5} style={{ paddingTop: '12px' }}>
                    <h2 className={styles.summaryHeading}>
                      {prod.product_name}
                    </h2>
                  </Col>
                  <Col lg={3} style={{ paddingTop: '12px' }}>
                    <p className={styles.summaryDesc}>
                      <span
                        style={{
                          fontSize: '12px',
                          lineHeight: '16px',
                          color: '#0F1111',
                          fontWeight: '500',
                        }}
                      >
                        Cantidad:
                      </span>{' '}
                      {prod.quantity}
                      <br />
                      <span
                        style={{
                          fontSize: '12px',
                          lineHeight: '16px',
                          color: '#0F1111',
                          fontWeight: '500',
                        }}
                      >
                        Peso:
                      </span>{' '}
                      {prod.weight} {prod.weight_unit}
                      <br />
                      <span
                        style={{
                          fontSize: '12px',
                          lineHeight: '16px',
                          color: '#0F1111',
                          fontWeight: '500',
                        }}
                      >
                        Unidad:
                      </span>{' '}
                      {prod.unit_in_package}
                    </p>
                  </Col>
                  <Col lg={2} style={{ paddingTop: '12px' }}>
                    <p
                      className={styles.summaryDesc}
                      style={{ float: 'right' }}
                    >
                      {prod.left_quantity}
                    </p>
                  </Col>
                </Row>
              </div>
            )
          )}
        </div>
      </div>
    </div>
  )
}

export default Leftover
