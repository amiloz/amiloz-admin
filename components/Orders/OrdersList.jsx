import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap'
import styles from './index.module.css'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import { toast } from 'react-toastify'
import AssignDeliveryRepModal from './Modals/AssignDeliveryRepModal'
import ViewSummaryModal from './Modals/ViewSummaryModal'
import DropdownTask from './DropdownTask'
import { useRouter } from 'next/dist/client/router'
import Pagination from 'react-js-pagination'
import withAuth from '../../hoc/withAuth'
import OrderEditModal from './Modals/OrderEditModal'
import OrderDeleteModal from './Modals/OrderDeleteModal'

const OrdersList = () => {
  const [showModal, setShowModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const [showSummaryModal, setShowSummaryModal] = useState(false)
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [industries, setIndustries] = useState([])
  const [selectedName, setSelectedName] = useState({})
  const [displaySortMenu, setDisplaySortMenu] = useState(false)
  const [filterKey, setFilterKey] = useState(null)
  const [sortKey, setSortKey] = useState(null)
  const [sortDir, setSortDir] = useState(true)
  const [date, setDate] = useState('')
  const [stores, setStores] = useState([])
  const [storeId, setStoreId] = useState('')
  const [selectedOrders, setSelectedOrders] = useState([])
  const [selectedOrdersData, setSelectedOrdersData] = useState([])
  const [selectAllOrders, setSelectAllOrders] = useState(false)
  const [selectedDeliveryRep, setSelectedDeliveryRep] = useState('')
  const [deliveryReps, setDeliveryReps] = useState([])
  const [productsSummary, setProductsSummary] = useState([])
  const [windowWidth, setWindowWidth] = useState(0)
  const [assignLoader, setAssignLoader] = useState(false)
  const [settleLoader, setSettleLoader] = useState(false)
  const [activePage, setActivePage] = useState(1)
  const [total, setTotal] = useState(0)
  const [products, setProducts] = useState([])
  const router = useRouter()

  useEffect(() => {
    setWindowWidth(window.innerWidth)

    return () => {
      setWindowWidth(window.innerWidth)
    }
  }, [])

  function useOutsideAlerterSort(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplaySortMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const sortRef = useRef(null)
  useOutsideAlerterSort(sortRef)

  const getAllIndusries = (
    dateValue,
    page = 1,
    storeIdValue,
    sortKeyValue,
    sortDirection
  ) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/orders?_limit=30&_page=${page}`
    if (dateValue) {
      url += `&date=${moment(dateValue).format('YYYY-MM-DD')}`
    }
    if (storeIdValue) {
      url += `&store_id=${storeIdValue}`
    }
    if (sortKeyValue) {
      const dir = sortDirection === true ? 'desc' : 'asc'
      url += `&sort_by=${sortKeyValue}&sort_dir=${dir}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setIndustries(res.data.orders)
      setTotal(res.data.total)
    })
  }
  const getAllProductsInitial = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/products?_limit=30&_page=1`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setProducts(res.data.products))
  }
  const getAllProducts = (e) => {
    const token = JSON.parse(localStorage.getItem('token'))
    const { value } = e.target
    let url = `/products?_limit=30&_page=1`
    if (value) {
      url += `&name=${value}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setProducts(res.data.products))
  }
  const getAllStores = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/stores`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setStores(res.data.stores))
  }
  const getAllDeliveryReps = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/users/all?role=delivery_rep`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setDeliveryReps(res.data.users))
  }
  useEffect(() => {
    getAllIndusries()
    getAllDeliveryReps()
    getAllStores()
    getAllProductsInitial()
  }, [])
  const sortKeyChangeHandler = (dir, e) => {
    const { value } = e.target
    if (e.target.checked) {
      setSortKey(value)
      setSortDir(dir)
      getAllIndusries(null, 1, filterKey, value, dir)
    } else {
      setSortKey(null)
      getAllIndusries(null, 1, filterKey, null)
    }
    setDisplaySortMenu(false)
  }
  const selectedOrdersChangeHandler = (id, industry, e) => {
    if (e.target.checked) {
      const selectedArr = [...selectedOrders.filter((com) => com !== ''), id]
      const selectedArrData = [
        ...selectedOrdersData.filter((com) => com !== ''),
        industry,
      ]
      setSelectedOrders(selectedArr)
      setSelectedOrdersData(selectedArrData)
    } else {
      const filterArr = selectedOrders.filter((pc) => pc !== id)
      const filterArrData = selectedOrdersData.filter((pc) => pc.id !== id)
      setSelectedOrders(filterArr)
      setSelectedOrdersData(filterArrData)
    }
  }
  const allOrdersChangeHandler = (e) => {
    if (e.target.checked) {
      const allOrders = industries?.map((ind) => ind.id)
      setSelectedOrders(allOrders)
      setSelectAllOrders(true)
      setSelectedOrdersData(industries)
    } else {
      setSelectedOrders([])
      setSelectedOrdersData([])
      setSelectAllOrders(false)
    }
  }
  const assignDeliveryRep = (e) => {
    e.preventDefault()
    setAssignLoader(true)
    axios({
      method: 'POST',
      url: `/orders/assign-delivery-rep`,
      data: {
        delivery_rep_id: selectedDeliveryRep,
        order_ids: selectedOrders,
      },
    })
      .then((res) => {
        setShowModal(false)
        setSelectedOrders([])
        setSelectAllOrders(false)
        setSelectedDeliveryRep('')
        toast('Delivery rep assigned!')
        getAllIndusries()
        setAssignLoader(false)
      })
      .catch((err) => {
        setAssignLoader(false)
        toast('Error assigning delivery rep')
      })
  }
  const viewSummary = () => {
    setShowSummaryModal(true)
    axios({
      method: 'POST',
      url: `/orders/summary`,
      data: {
        order_ids: selectedOrders,
      },
    }).then((res) => {
      setProductsSummary(res.data.products)
    })
  }
  const settleOrder = () => {
    setSettleLoader(true)
    axios({
      method: 'POST',
      url: `/orders/settle`,
      data: {
        order_ids: selectedOrders,
      },
    })
      .then((res) => {
        toast('Order settled marked!')
        getAllIndusries(date, 1, filterKey, sortKey, sortDir)
        setSettleLoader(false)
        setShowSummaryModal(false)
      })
      .catch((err) => {
        toast('Error settling order(s)')
        setSettleLoader(false)
      })
  }
  console.log(selectedOrders, selectAllOrders)
  return (
    <div
      className={styles.Container}
      style={industries.length === 0 ? { marginLeft: '0' } : {}}
    >
      <Row>
        <Col>
          <div style={{ position: 'relative' }}>
            <button
              type="button"
              onClick={() => setShowModal(true)}
              className={styles.buttonAdd}
              disabled={selectedOrders.length === 0}
              style={
                selectedOrders.length === 0
                  ? { background: '#e7e7e7', cursor: 'not-allowed' }
                  : {}
              }
            >
              Asignar repartidor
            </button>
            <button
              type="button"
              onClick={() => viewSummary()}
              className={styles.buttonAdd}
              disabled={selectedOrders.length === 0}
              style={
                selectedOrders.length === 0
                  ? { background: '#e7e7e7', cursor: 'not-allowed' }
                  : {}
              }
            >
              Resumen
            </button>
          </div>
        </Col>
        <Col>
          <div className={styles.filterSortDiv}>
            <div style={{ marginRight: '10px' }}>
              <DatePicker
                selected={date}
                onChange={(date) => {
                  setDate(date)
                  getAllIndusries(date, 1, storeId)
                }}
                placeholderText="Filtrar por fecha"
                className={styles.modalInputDate}
              />
            </div>
            <div>
              {/* <label>Filter by Store</label> */}
              <select
                value={storeId}
                onChange={(e) => {
                  setStoreId(e.target.value)
                  getAllIndusries(date, 1, e.target.value)
                }}
                className={styles.modalInput}
                style={{ height: '32px', paddingTop: '0', paddingBottom: '0' }}
              >
                <option value="">All</option>
                {stores?.map((store) => (
                  <option key={store.id} value={store.id}>
                    {store.name}
                  </option>
                ))}
              </select>
            </div>
            <div className={styles.sortByDiv}>
              <span onClick={() => setDisplaySortMenu(!displaySortMenu)}>
                Ordenar por{' '}
                <span style={{ float: 'right' }}>
                  <i className="fa fa-caret-down" aria-hidden="true"></i>
                </span>
              </span>
              {displaySortMenu ? (
                <ul ref={sortRef}>
                  <li
                    className={
                      sortKey === 'date' && !sortDir && styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="date"
                          checked={sortKey === 'date' && !sortDir}
                          onChange={(e) => sortKeyChangeHandler(false, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Expected Delivery Date (Ascending)
                    </label>
                  </li>
                  <li
                    className={
                      sortKey === 'date' && sortDir && styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="date"
                          checked={sortKey === 'date' && sortDir}
                          onChange={(e) => sortKeyChangeHandler(true, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Expected Delivery Date (Descending)
                    </label>
                  </li>
                </ul>
              ) : null}
            </div>
          </div>
        </Col>
      </Row>
      {windowWidth > 800 ? (
        <>
          <div className={styles.myTableParent}>
            <table className={styles.myTable}>
              <thead>
                <tr>
                  <th className={styles.theadTh1}>
                    <input
                      type="checkbox"
                      onChange={(e) => allOrdersChangeHandler(e)}
                      checked={selectAllOrders}
                    />
                  </th>
                  <th className={styles.theadTh1}>Pedido Id </th>
                  <th className={styles.theadTh1}>Fecha</th>
                  <th className={styles.theadTh1}>Fecha estimada de entrega</th>
                  <th className={styles.theadTh}>Tiendas</th>
                  <th className={styles.theadTh1}>Total</th>
                  <th className={styles.theadTh1}>Estatus</th>
                  <th className={styles.theadTh1}>Entregado</th>
                  <th className={styles.theadTh1}>Repartidor</th>
                  <th className={styles.theadTh1}>Vendedor</th>
                  <th className={styles.theadTh1}>Ver detalles</th>
                  <th className={styles.theadTh1}>Acción</th>
                </tr>
              </thead>
              <tbody>
                {industries?.map((industry) => {
                  const {
                    id,
                    date,
                    grand_total,
                    status,
                    sales_rep_name,
                    store,
                    delivery_rep_name,
                    created_at,
                    is_settled,
                  } = industry
                  return (
                    <tr
                      key={id}
                      className={styles.tbodyTr}
                      style={{ margin: '12px 0' }}
                    >
                      <td className={styles.tbodyTd1}>
                        <input
                          type="checkbox"
                          onChange={(e) =>
                            selectedOrdersChangeHandler(id, industry, e)
                          }
                          checked={selectedOrders.includes(id)}
                          // disabled={selectAllOrders}
                        />
                      </td>
                      <td className={styles.tbodyTd1}>{id}</td>
                      <td className={styles.tbodyTd1}>
                        {moment(created_at).format('LL')}
                      </td>
                      <td className={styles.tbodyTd1}>
                        {moment(date).format('LL')}
                      </td>
                      <td className={styles.tbodyTd}>
                        <div>
                          <span className={styles.OrderName}>{store.name}</span>
                          <span className={styles.OrderContact}>
                            {store.mobile}
                          </span>
                          <br />
                          <span className={styles.OrderName}>
                            {store.poc_name}
                          </span>
                          <span className={styles.OrderContact}>
                            {store.poc_mobile}
                          </span>
                          <br />
                          <span className={styles.OrderContact}>
                            {store.street}, {store.city}, {store.state},{' '}
                            {store.country}
                          </span>
                        </div>
                      </td>
                      <td className={styles.tbodyTd1}>{grand_total}</td>
                      <td className={styles.tbodyTd1}>{status}</td>
                      <td className={styles.tbodyTd1}>
                        {is_settled === 1 ? 'Yes' : 'No'}
                      </td>
                      <td className={styles.tbodyTd1}>{delivery_rep_name}</td>
                      <td className={styles.tbodyTd1}>{sales_rep_name}</td>
                      <td className={styles.tbodyTd1}>
                        <button
                          onClick={() => {
                            router.push(`/orders/${id}`)
                          }}
                          className={styles.buttonEditProd}
                        >
                          Ver detalles
                        </button>
                      </td>
                      <td className={styles.tbodyTd1}>
                        <button
                          onClick={() => {
                            setShowEditModal(true)
                            setSelectedName(industry)
                          }}
                          className={styles.buttonEditProd}
                          disabled={
                            !(status === 'Assigned' || status === 'Placed')
                          }
                          style={
                            status === 'Assigned' || status === 'Placed'
                              ? {}
                              : { background: '#e7e7e7', cursor: 'not-allowed' }
                          }
                        >
                          Editar
                        </button>
                        <button
                          onClick={() => {
                            setShowDeleteModal(true)
                            setSelectedName(industry)
                          }}
                          disabled={status === 'Cancelled'}
                          style={
                            status === 'Cancelled'
                              ? { background: '#e7e7e7', cursor: 'not-allowed' }
                              : {}
                          }
                          className={styles.buttonDeleteProd}
                        >
                          Cancelar
                        </button>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        </>
      ) : (
        <>
          <div>
            {industries?.map((industry) => {
              const { id, date, grand_total, status, is_settled, store } =
                industry
              return (
                <div key={id} className={styles.tableMobDiv}>
                  <div className={styles.tableMobDivFlex}>
                    <input
                      type="checkbox"
                      onChange={(e) =>
                        selectedOrdersChangeHandler(id, industry, e)
                      }
                      checked={selectedOrders.includes(id)}
                      style={{ marginRight: '10px' }}
                      // disabled={selectAllOrders}
                    />
                    <span className={styles.tableMobDivFlex1}>
                      {moment(date).format('LL')}
                    </span>
                    <span className={styles.tableMobDivFlex2}>
                      <DropdownTask
                        openEditModal={() => {
                          setShowEditModal(true)
                          setSelectedName(industry)
                        }}
                        openDeleteModal={() => {
                          setShowDeleteModal(true)
                          setSelectedName(industry)
                        }}
                        selectedOrder={industry}
                      />
                    </span>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Tienda: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>
                      <div>
                        <span className={styles.OrderName}>{store.name}</span>
                        <span className={styles.OrderContact}>
                          {store.mobile}
                        </span>
                        <br />
                        <span className={styles.OrderName}>
                          {store.poc_name}
                        </span>
                        <span className={styles.OrderContact}>
                          {store.poc_mobile}
                        </span>
                        <br />
                        <span className={styles.OrderContact}>
                          {store.street}, {store.city}, {store.state},{' '}
                          {store.country}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Total: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>
                      {grand_total}
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Estatus: </span>
                    <div className={styles.tableMobDivDownInfo}>{status}</div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Entregado: </span>
                    <div className={styles.tableMobDivDownInfo}>
                      {is_settled === 1 ? 'Yes' : 'No'}
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <div className={styles.tableMobDivDownInfo}>
                      <button
                        onClick={() => {
                          router.push(`/orders/${id}`)
                        }}
                        className={styles.buttonEditProd}
                      >
                        Ver detalles
                      </button>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </>
      )}
      <div
        style={{
          marginTop: '20px',
          borderTop: '1px solid #e7e7e7',
          textAlign: 'right',
          paddingTop: '20px',
          minHeight: '62px',
        }}
      >
        <div style={{ float: 'right' }}>
          <Pagination
            activePage={activePage}
            itemsCountPerPage={30}
            totalItemsCount={total}
            pageRangeDisplayed={5}
            onChange={(page) => {
              setActivePage(page)
              getAllIndusries(date, page, filterKey, sortKey, sortDir)
            }}
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>
      </div>
      <AssignDeliveryRepModal
        show={showModal}
        onHide={() => setShowModal(false)}
        handleClose={() => setShowModal(false)}
        submitHandler={assignDeliveryRep}
        setSelectedDeliveryRep={setSelectedDeliveryRep}
        deliveryReps={deliveryReps}
        loading={assignLoader}
      />
      <ViewSummaryModal
        show={showSummaryModal}
        onHide={() => setShowSummaryModal(false)}
        handleClose={() => setShowSummaryModal(false)}
        productsSummary={productsSummary}
        settleOrder={settleOrder}
        settleLoader={settleLoader}
        selectedOrdersData={selectedOrdersData}
      />
      <OrderEditModal
        show={showEditModal}
        onHide={() => setShowEditModal(false)}
        handleClose={() => setShowEditModal(false)}
        getAllIndusries={getAllIndusries}
        selectedIndustry={selectedName}
        stores={stores}
        productsList={products}
        getAllProducts={getAllProducts}
        getAllStores={getAllStores}
      />
      <OrderDeleteModal
        show={showDeleteModal}
        onHide={() => setShowDeleteModal(false)}
        handleClose={() => setShowDeleteModal(false)}
        getAllIndusries={getAllIndusries}
        selectedIndustry={selectedName}
      />
    </div>
  )
}

export default withAuth(OrdersList)
