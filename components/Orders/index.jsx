import { useRouter } from 'next/dist/client/router'
import { useEffect, useState } from 'react'
import withAuth from '../../hoc/withAuth'
import OrdersList from './OrdersList'
import Leftover from './Leftover'

const Orders = () => {
  const router = useRouter()
  const [tab, setTab] = useState('')

  useEffect(() => {
    setTab(router.query.tab)
  }, [router.query.tab])

  let content = ''
  if (tab === 'orders') {
    content = <OrdersList />
  } else if (tab === 'left-over') {
    content = <Leftover />
  }

  return <>{content}</>
}

export default withAuth(Orders)
