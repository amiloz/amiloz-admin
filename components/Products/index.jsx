import { useRouter } from 'next/dist/client/router'
import { useEffect, useState } from 'react'
import styles from './index.module.css'
import Categories from './Categories'
import Brands from './Brands'
import Items from './Items'
import withAuth from '../../hoc/withAuth'

const ProductsRoute = () => {
  const router = useRouter()
  const [tab, setTab] = useState('')

  useEffect(() => {
    setTab(router.query.tab)
  }, [router.query.tab])

  let content = ''
  if (tab === 'brands') {
    content = <Brands />
  } else if (tab === 'categories') {
    content = <Categories />
  } else if (tab === 'items') {
    content = <Items />
  }

  return <div className={styles.Container}>{content}</div>
}

export default withAuth(ProductsRoute)
