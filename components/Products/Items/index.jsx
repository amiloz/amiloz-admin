import axios from 'axios'
import moment from 'moment'
import { useEffect, useRef, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap'
import styles from '../index.module.css'
import DropdownTask from './DropdownTask'
import ItemsCreateModal from './ItemsCreateModal'
import Sidebar from './Sidebar'
import Pagination from 'react-js-pagination'

const Items = () => {
  const [showModal, setShowModal] = useState(false)
  const [showSidebar, setShowSidebar] = useState(false)
  const [industries, setIndustries] = useState([])
  const [windowWidth, setWindowWidth] = useState(0)
  const [displaySortMenu, setDisplaySortMenu] = useState(false)
  const [sortKey, setSortKey] = useState(null)
  const [sortDir, setSortDir] = useState(false)
  const [brandsList, setBrandsList] = useState([])
  const [categoryList, setCategoryList] = useState([])
  const [productData, setProductData] = useState(null)
  const [brandId, setBrandId] = useState('')
  const [categoryId, setCategoryId] = useState('')
  const [availableFilter, setAvailableFilter] = useState('')
  const [activePage, setActivePage] = useState(1)
  const [total, setTotal] = useState(0)

  function useOutsideAlerterSort(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplaySortMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const sortRef = useRef(null)
  useOutsideAlerterSort(sortRef)

  useEffect(() => {
    setWindowWidth(window.innerWidth)

    return () => {
      setWindowWidth(window.innerWidth)
    }
  }, [])

  const getAllIndusries = (
    query,
    page = 1,
    brandFilterId,
    categoryFilterId,
    availableFilterId,
    sortKeyValue,
    sortDirection
  ) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/products?_limit=30&_page=${page}`
    if (query) {
      url += `&name=${encodeURIComponent(query)}`
    }
    if (brandFilterId) {
      url += `&brand_id=${brandFilterId}`
    }
    if (categoryFilterId) {
      url += `&category_id=${categoryFilterId}`
    }
    if (availableFilterId) {
      url += `&available=${availableFilterId}`
    }
    if (sortKeyValue) {
      const dir = sortDirection === true ? 'desc' : 'asc'
      url += `&sort_by=${sortKeyValue}&sort_dir=${dir}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setIndustries(res.data.products)
      setTotal(res.data.total)
    })
  }
  const clearFilter = () => {
    getAllIndusries(null, 1, null, null, null, sortKey, sortDir)
    setActivePage(1)
    setBrandId('')
    setCategoryId('')
    setAvailableFilter('')
  }
  const getAllBrands = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/brands`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setBrandsList(res.data.brands))
  }
  const getAllCategories = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/categories`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setCategoryList(res.data.categories))
  }
  useEffect(() => {
    getAllIndusries()
    getAllBrands()
    getAllCategories()
  }, [])
  const sortKeyChangeHandler = (dir, e) => {
    const { value } = e.target
    if (e.target.checked) {
      setSortKey(value)
      setSortDir(dir)
      getAllIndusries(null, 1, brandId, categoryId, availableFilter, value, dir)
      setActivePage(1)
    } else {
      setSortKey(null)
      getAllIndusries(null, 1, brandId, categoryId, availableFilter, null)
      setActivePage(1)
    }
    setDisplaySortMenu(false)
  }
  const brandIdChangeHandler = (id) => {
    setBrandId(id)
    getAllIndusries(null, 1, id, categoryId, availableFilter, sortKey, sortDir)
    setActivePage(1)
  }
  const categoryIdChangeHandler = (id) => {
    setCategoryId(id)
    getAllIndusries(null, 1, brandId, id, availableFilter, sortKey, sortDir)
    setActivePage(1)
  }
  const availableFilterChangeHandler = (filter) => {
    setAvailableFilter(filter)
    getAllIndusries(null, 1, brandId, categoryId, filter, sortKey, sortDir)
    setActivePage(1)
  }
  return (
    <>
      <Row>
        <Col>
          <div style={{ position: 'relative' }}>
            <input
              className={styles.searchInput}
              placeholder="Buscar productos"
              onChange={(e) =>
                getAllIndusries(
                  e.target.value,
                  1,
                  brandId,
                  categoryId,
                  availableFilter,
                  sortKey,
                  sortDir
                )
              }
            />
            <Image
              src="/images/searchIcon.png"
              alt=""
              height="16px"
              width="16px"
              className={styles.searchInputIcon}
            />
          </div>
        </Col>
        <Col>
          <div className={styles.filterSortDiv}>
            <button
              type="button"
              onClick={() => {
                setShowModal(true)
                setProductData({ modalType: 'ADD', industry: '' })
              }}
              className={styles.buttonAdd}
            >
              + Agregar productos
            </button>
            <div className={styles.sortByDiv}>
              <div onClick={() => setDisplaySortMenu(!displaySortMenu)}>
                Ordenar por{' '}
                <span style={{ float: 'right' }}>
                  <i className="fa fa-caret-down" aria-hidden="true"></i>
                </span>
              </div>
              {displaySortMenu ? (
                <ul ref={sortRef}>
                  <li
                    className={
                      sortKey === 'selling_price' &&
                      !sortDir &&
                      styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="selling_price"
                          checked={sortKey === 'selling_price' && !sortDir}
                          onChange={(e) => sortKeyChangeHandler(false, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Price (Low to High)
                    </label>
                  </li>
                  <li
                    className={
                      sortKey === 'selling_price' &&
                      sortDir &&
                      styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="selling_price"
                          checked={sortKey === 'selling_price' && sortDir}
                          onChange={(e) => sortKeyChangeHandler(true, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Price (High to Low)
                    </label>
                  </li>
                </ul>
              ) : null}
            </div>
            <div className={styles.filterByDiv}>
              <span onClick={() => setShowSidebar(!showSidebar)}>
                Filtrar por{' '}
              </span>
            </div>
          </div>
        </Col>
      </Row>
      {showSidebar && (
        <Sidebar
          setShowSidebar={setShowSidebar}
          brands={brandsList}
          categories={categoryList}
          brandId={brandId}
          categoryId={categoryId}
          availableFilter={availableFilter}
          brandIdChangeHandler={brandIdChangeHandler}
          categoryIdChangeHandler={categoryIdChangeHandler}
          availableFilterChangeHandler={availableFilterChangeHandler}
          clearFilter={clearFilter}
        />
      )}
      {windowWidth > 800 ? (
        <>
          <table className={styles.myTable} style={{ marginBottom: '5px' }}>
            <colgroup>
              <col span="1" style={{ width: '15%' }} />
              <col span="1" style={{ width: '30%' }} />
              <col span="1" style={{ width: '20%' }} />
              <col span="1" style={{ width: '7%' }} />
              <col span="1" style={{ width: '7%' }} />
              <col span="1" style={{ width: '7%' }} />
              <col span="1" style={{ width: '14%' }} />
            </colgroup>
            <thead>
              <tr>
                <th> </th>
                <th className={styles.theadTh}>Información del producto</th>
                <th className={styles.theadTh}>Especificaciones</th>
                <th className={styles.theadTh1}>Precio de Compra</th>
                <th className={styles.theadTh1}>Precio de Venta</th>
                <th className={styles.theadTh1}>Disponible</th>
                <th className={styles.theadTh1}>Acción</th>
              </tr>
            </thead>
            <tbody>
              {industries?.map((industry) => {
                const {
                  id,
                  selling_price,
                  name,
                  images,
                  brand_name,
                  cost_price,
                  description,
                  quantity,
                  size,
                  sku,
                  unit_in_package,
                  weight,
                  weight_unit,
                  available,
                  primary_image,
                } = industry
                return (
                  <tr key={id}>
                    <td className={styles.tbodyTd1}>
                      <Image
                        src={primary_image}
                        alt=""
                        width="70px"
                        height="70px"
                      />
                    </td>
                    <td className={styles.tbodyTd}>
                      <div
                        style={{
                          fontWeight: '500',
                          fontSize: '16px',
                          lineHeight: '24px',
                          letterSpacing: '0.15px',
                          color: '#2D4C66',
                        }}
                      >
                        {name}
                      </div>
                      <div
                        style={{
                          fontWeight: '500',
                          fontSize: '14px',
                          lineHeight: '24px',
                          letterSpacing: '0.15px',
                          color: '#BDBDBD',
                        }}
                      >
                        {brand_name}
                      </div>
                      <div
                        style={{
                          fontWeight: '400',
                          fontSize: '14px',
                          lineHeight: '20px',
                          letterSpacing: '0.15px',
                          color: '#6F87A0',
                        }}
                      >
                        <em>
                          {description && description.length > 100
                            ? description.substring(0, 100 - 3) + `   ...`
                            : description}
                        </em>
                      </div>
                    </td>
                    <td className={styles.tbodyTd}>
                      <div
                        className={styles.tbodyTdDiv}
                        style={{ marginBottom: '3px' }}
                      >
                        <span className={styles.tbodyTdSpan}>Cantidad</span>
                        {quantity}
                      </div>
                      <div
                        className={styles.tbodyTdDiv}
                        style={{ marginBottom: '3px' }}
                      >
                        <span className={styles.tbodyTdSpan}>Peso</span>
                        {weight} {weight_unit}
                      </div>
                      <div
                        className={styles.tbodyTdDiv}
                        style={{ marginBottom: '3px' }}
                      >
                        <span className={styles.tbodyTdSpan}>Unidad</span>
                        {unit_in_package}
                      </div>
                    </td>
                    <td className={styles.tbodyTd1}>${cost_price}</td>
                    <td className={styles.tbodyTd1}>${selling_price}</td>
                    <td className={styles.tbodyTd1}>
                      <i
                        className="fa fa-circle"
                        aria-hidden="true"
                        style={
                          available > 41
                            ? { color: 'green' }
                            : available <= 40 && available >= 16
                            ? { color: 'yellow' }
                            : { color: 'red' }
                        }
                      ></i>
                      &nbsp;&nbsp;
                      {available}
                    </td>
                    <td className={styles.tbodyTd1}>
                      <button
                        type="button"
                        onClick={() => {
                          setShowModal(true)
                          setProductData({
                            modalType: 'EDIT',
                            industry: industry,
                          })
                        }}
                        className={styles.buttonEditProd}
                      >
                        Editar producto
                      </button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </>
      ) : (
        <>
          <div>
            {industries?.map((industry) => {
              const {
                id,
                selling_price,
                name,
                images,
                brand_name,
                cost_price,
                description,
                quantity,
                unit_in_package,
                weight,
                weight_unit,
              } = industry
              return (
                <div key={id} className={styles.tableMobDiv}>
                  <div className={styles.tableMobDivFlex}>
                    <span className={styles.tableMobDivFlex1}>{name}</span>
                    <span className={styles.tableMobDivFlex2}>
                      <DropdownTask
                        setShowModal={setShowModal}
                        setProductData={setProductData}
                        industry={industry}
                      />
                    </span>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Especificaciones: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>
                      <div className={styles.tbodyTdDiv}>
                        <span className={styles.tbodyTdSpan}>Cantidad</span>
                        {quantity}
                      </div>
                      <div className={styles.tbodyTdDiv}>
                        <span className={styles.tbodyTdSpan}>Peso</span>
                        {weight} {weight_unit}
                      </div>
                      <div className={styles.tbodyTdDiv}>
                        <span className={styles.tbodyTdSpan}>Unidad</span>
                        {unit_in_package}
                      </div>
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Precio de Venta: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>
                      {selling_price}
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Precio de Compra: </span>
                    <div className={styles.tableMobDivDownInfo}>
                      {cost_price}
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </>
      )}
      <div
        style={{
          marginTop: '20px',
          borderTop: '1px solid #e7e7e7',
          textAlign: 'right',
          paddingTop: '20px',
          minHeight: '62px',
        }}
      >
        <div style={{ float: 'right' }}>
          <Pagination
            activePage={activePage}
            itemsCountPerPage={30}
            totalItemsCount={total}
            pageRangeDisplayed={5}
            onChange={(page) => {
              setActivePage(page)
              getAllIndusries(
                null,
                page,
                brandId,
                categoryId,
                availableFilter,
                sortKey,
                sortDir
              )
            }}
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>
      </div>
      <ItemsCreateModal
        show={showModal}
        onHide={() => setShowModal(false)}
        handleClose={() => setShowModal(false)}
        getAllIndusries={getAllIndusries}
        brandsList={brandsList}
        categoryList={categoryList}
        productData={productData}
        windowWidth={windowWidth}
        activePageFilter={activePage}
        brandIdFilter={brandId}
        categoryIdFilter={categoryId}
        availableFilter={availableFilter}
        sortKeyFilter={sortKey}
        sortDirFilter={sortDir}
      />
    </>
  )
}

export default Items
