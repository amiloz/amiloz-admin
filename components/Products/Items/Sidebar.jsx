import { Image } from 'react-bootstrap'
import styles from '../index.module.css'

function Sidebar({
  setShowSidebar,
  brands,
  categories,
  brandId,
  categoryId,
  brandIdChangeHandler,
  categoryIdChangeHandler,
  availableFilter,
  availableFilterChangeHandler,
  clearFilter,
}) {
  return (
    <div className={styles.sidebar}>
      <h2
        className={styles.productsCardName}
        style={{ fontWeight: '500', fontSize: '20px', lineHeight: '24px' }}
      >
        Filtrar por
      </h2>
      <button
        onClick={() => setShowSidebar(false)}
        className={styles.modalCloseButton}
      >
        <Image src="/images/close_small.svg" alt="X" />
      </button>
      <button
        onClick={() => clearFilter()}
        className={styles.sidebarClearButton}
      >
        Filtros claros
      </button>
      <label className={styles.modalLabel} style={{ marginTop: '15px' }}>
        Categorias
      </label>
      <select
        className={styles.modalInput}
        value={categoryId}
        onChange={(e) => categoryIdChangeHandler(e.target.value)}
      >
        <option value="">Todos</option>
        {categories?.map((cat) => (
          <option key={cat.id} value={cat.id}>
            {cat.name}
          </option>
        ))}
      </select>
      <label className={styles.modalLabel} style={{ marginTop: '15px' }}>
        Marcas
      </label>
      <select
        className={styles.modalInput}
        value={brandId}
        onChange={(e) => brandIdChangeHandler(e.target.value)}
      >
        <option value="">Todos</option>
        {brands?.map((cat) => (
          <option key={cat.id} value={cat.id}>
            {cat.name}
          </option>
        ))}
      </select>
      <label className={styles.modalLabel} style={{ marginTop: '15px' }}>
        Disponibilidad
      </label>
      <select
        className={styles.modalInput}
        value={availableFilter}
        onChange={(e) => availableFilterChangeHandler(e.target.value)}
      >
        <option value="">Todos</option>
        <option value="high">Alto</option>
        <option value="medium">Medio</option>
        <option value="low">Bajo</option>
      </select>
    </div>
  )
}

export default Sidebar
