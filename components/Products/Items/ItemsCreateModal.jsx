import axios from 'axios'
import { useState, useEffect } from 'react'
import { Col, Form, Image, Modal, Row } from 'react-bootstrap'
import styles from '../index.module.css'
import Dropzone from '../../UI/Dropzone'
import { toast } from 'react-toastify'

const ItemsCreateModal = ({
  show,
  handleClose,
  getAllIndusries,
  brandsList,
  categoryList,
  productData,
  windowWidth,
  activePageFilter,
  brandIdFilter,
  categoryIdFilter,
  availableFilter,
  sortKeyFilter,
  sortDirFilter,
}) => {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [image, setImage] = useState([])
  const [brandId, setBrandId] = useState('')
  const [categoryId, setCategoryId] = useState('')
  const [sellingPrice, setSellingPrice] = useState('')
  const [costPrice, setCostPrice] = useState('')
  const [quantity, setQuantity] = useState('')
  const [size, setSize] = useState('')
  const [weight, setWeight] = useState('')
  const [unitPackage, setUnitPackage] = useState('')
  const [sku, setSku] = useState('')
  const [loading, setLoading] = useState(false)
  const [weightUnit, setWeightUnit] = useState('')
  useEffect(() => {
    if (show) setName(productData?.industry.name || '')
    setDescription(productData?.industry.description || '')
    setImage(
      productData?.industry.images
        ? productData?.industry.images.map((item, idx) => {
            return {
              link: item.url,
              name: `${idx}.png`,
              size: 276386,
              type: 'image/png',
            }
          })
        : []
    )
    setBrandId(productData?.industry.brand_id || '')
    setCategoryId(productData?.industry.category_id || '')
    setSellingPrice(productData?.industry.selling_price || '')
    setCostPrice(productData?.industry.cost_price || '')
    setQuantity(productData?.industry.quantity || '')
    setSize(productData?.industry.size || '')
    setWeight(productData?.industry.weight || '')
    setWeightUnit(productData?.industry.weight_unit || '')
    setUnitPackage(productData?.industry.unit_in_package || '')
    setSku(productData?.industry.sku || '')
  }, [show])
  const submitHandler = (e) => {
    if (productData.modalType === 'ADD') {
      addItem(e)
    } else editItem(e)
  }
  const addItem = (e) => {
    e.preventDefault()
    if (image.length === 0) {
      toast('Please select at least 1 image')
      setLoading(false)
      return false
    }
    setLoading(true)
    axios({
      method: 'POST',
      url: `/products`,
      data: {
        name,
        ...(description && { description }),
        images: image?.map((i) => i.link),
        published: true,
        brand_id: brandId,
        selling_price: sellingPrice,
        cost_price: costPrice,
        quantity,
        weight,
        weight_unit: weightUnit,
        unit_in_package: unitPackage,
        category_id: categoryId,
        primary_image: image[0].link,
      },
    })
      .then((res) => {
        getAllIndusries(
          null,
          activePageFilter,
          brandIdFilter,
          categoryIdFilter,
          availableFilter,
          sortKeyFilter,
          sortDirFilter
        )
        setLoading(false)
        closeModalAndClearState()
        toast('Product added')
      })
      .catch((err) => {
        setLoading(false)
        toast('Error adding product')
      })
  }

  const editItem = (e) => {
    e.preventDefault()
    if (image.length === 0) {
      toast('Please select at least 1 image')
      setLoading(false)
      return false
    }
    setLoading(true)
    axios({
      method: 'PUT',
      url: `/products/${productData.industry.id}`,
      data: {
        name,
        ...(description && { description }),
        images: image?.map((i) => i.link),
        published: true,
        brand_id: brandId,
        selling_price: sellingPrice,
        cost_price: costPrice,
        quantity,
        weight,
        weight_unit: weightUnit,
        unit_in_package: unitPackage,
        category_id: categoryId,
        primary_image: image[0].link,
      },
    })
      .then((res) => {
        getAllIndusries(
          null,
          activePageFilter,
          brandIdFilter,
          categoryIdFilter,
          availableFilter,
          sortKeyFilter,
          sortDirFilter
        )
        toast('Producto editado')
        setLoading(false)
        closeModalAndClearState()
      })
      .catch((err) => {
        setLoading(false)
        toast('Error al editar el producto')
      })
  }
  const handleImageAdd = (url, selectedFile) => {
    let imageArray = image
    const newObj = {
      type: selectedFile.type,
      link: url,
      size: selectedFile.size,
      name: selectedFile.name,
    }
    imageArray = [...imageArray, newObj]

    setImage(imageArray)
  }

  const removeImageId = (link) => {
    console.log('agigyigy', link)
    let imageArray = image
    const newArray = imageArray.filter((item) => item.link !== link)
    console.log('igigyigy', newArray)
    setImage(newArray)
  }
  const closeModalAndClearState = () => {
    handleClose()
    setDescription('')
    setBrandId('')
    setSku('')
    setSellingPrice('')
    setCostPrice('')
    setQuantity('')
    setSize('')
    setWeight('')
    setWeightUnit('')
    setUnitPackage('')
    setImage([])
    setName('')
    setCategoryId('')
  }
  return (
    <Modal
      show={show}
      onHide={closeModalAndClearState}
      fullscreen="md-down"
      backdrop="static"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
      size="xl"
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div
          className={styles.mobileModalDiv}
          onClick={() => closeModalAndClearState()}
        >
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            New Product
          </p>
        </div>
        <button
          type="button"
          onClick={() => closeModalAndClearState()}
          className={styles.modalCloseButton}
        >
          <Image src="/images/close.png" alt="X" />
        </button>
        <h3 className={styles.modalHeadingMain}>
          {productData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Productos
        </h3>
        <Form onSubmit={(e) => submitHandler(e)}>
          <Row
            style={
              windowWidth > 800
                ? {}
                : {
                    height: 'auto',
                    maxHeight: '70vh',
                    overflowY: 'auto',
                  }
            }
          >
            <Col>
              <div>
                <Form.Label className={styles.modalLabel}>
                  Nombre del producto{' '}
                  <span style={{ color: '#fc5447' }}>*</span>
                </Form.Label>
                <Form.Control
                  placeholder="Enter product name"
                  onChange={(e) => setName(e.target.value)}
                  value={name}
                  required
                  className={styles.modalInput}
                />
                <Row>
                  <Col>
                    <Form.Label className={styles.modalLabel}>
                      Categorias <span style={{ color: '#fc5447' }}>*</span>
                    </Form.Label>
                    <select
                      className={styles.modalInput}
                      style={{ width: '100%', display: 'block' }}
                      value={categoryId}
                      onChange={(e) => setCategoryId(e.target.value)}
                    >
                      <option hidden>-Seleccione-</option>
                      <option disabled selected>
                        -Seleccione-
                      </option>
                      {categoryList?.map((cat) => (
                        <option key={cat.id} value={cat.id}>
                          {cat.name}
                        </option>
                      ))}
                    </select>
                  </Col>
                  <Col>
                    <Form.Label className={styles.modalLabel}>
                      Marcas <span style={{ color: '#fc5447' }}>*</span>
                    </Form.Label>
                    <select
                      className={styles.modalInput}
                      style={{ width: '100%', display: 'block' }}
                      value={brandId}
                      onChange={(e) => setBrandId(e.target.value)}
                    >
                      <option hidden>-Seleccione-</option>
                      <option disabled selected>
                        -Seleccione-
                      </option>
                      {brandsList?.map((cat) => (
                        <option key={cat.id} value={cat.id}>
                          {cat.name}
                        </option>
                      ))}
                    </select>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Label className={styles.modalLabel}>
                      Precio de Venta{' '}
                      <span style={{ color: '#fc5447' }}>*</span>
                    </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter Selling Price"
                      onChange={(e) => setSellingPrice(e.target.value)}
                      value={sellingPrice}
                      required
                      className={styles.modalInput}
                    />
                  </Col>
                  <Col>
                    <Form.Label className={styles.modalLabel}>
                      Precio de Compra{' '}
                      <span style={{ color: '#fc5447' }}>*</span>
                    </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter Cost Price"
                      onChange={(e) => setCostPrice(e.target.value)}
                      value={costPrice}
                      required
                      className={styles.modalInput}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Label className={styles.modalLabel}>
                      Cantidad <span style={{ color: '#fc5447' }}>*</span>
                    </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter quantity"
                      onChange={(e) => setQuantity(e.target.value)}
                      value={quantity}
                      required
                      className={styles.modalInput}
                    />
                  </Col>
                  <Col>
                    <Form.Label className={styles.modalLabel}>
                      Unidad en paquete{' '}
                      <span style={{ color: '#fc5447' }}>*</span>
                    </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter unit"
                      onChange={(e) => setUnitPackage(e.target.value)}
                      value={unitPackage}
                      required
                      className={styles.modalInput}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Label className={styles.modalLabel}>
                      Peso <span style={{ color: '#fc5447' }}>*</span>
                    </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter Weight"
                      onChange={(e) => setWeight(e.target.value)}
                      value={weight}
                      required
                      className={styles.modalInput}
                    />
                  </Col>
                  <Col>
                    <Form.Label className={styles.modalLabel}>
                      Unidad de medida{' '}
                      <span style={{ color: '#fc5447' }}>*</span>
                    </Form.Label>
                    <select
                      value={weightUnit}
                      onChange={(e) => setWeightUnit(e.target.value)}
                      required
                      className={styles.modalInput}
                      style={{ width: '100%' }}
                    >
                      <option hidden>Seleccione</option>
                      <option selected disabled>
                        Seleccione
                      </option>
                      <option value="litres">Litres</option>
                      <option value="ml">Milli Litres</option>
                      <option value="kg">Kg</option>
                      <option value="grams">grams</option>
                    </select>
                  </Col>
                </Row>
                <Form.Label className={styles.modalLabel}>
                  Descripción
                </Form.Label>
                <Form.Control
                  placeholder="Enter description"
                  onChange={(e) => setDescription(e.target.value)}
                  value={description}
                  className={styles.modalInput}
                  as="textarea"
                />
              </div>
            </Col>
            <Col xl={4} lg={4} md={12} className={styles.itemImageDiv}>
              <>
                <Form.Label className={styles.modalLabel}>
                  Imagen del producto
                </Form.Label>
                <div>
                  <Dropzone
                    width="172px"
                    height="142px"
                    firstText="Drag files here or"
                    textToHighlight="browse"
                    lastText="to upload"
                    supportedTypes={['images']}
                    onSuccessImgSelectedFn={(url, selectedFile) =>
                      handleImageAdd(url, selectedFile)
                    }
                    documents={image}
                    typeOfUploading="products"
                    isMultiSelect={true}
                    previewHeight={'100px'}
                    previewWidth={'150px'}
                    removeItem={(link) => removeImageId(link)}
                  />
                </div>
              </>
            </Col>
          </Row>
          <div className={styles.modalButtonDiv}>
            <button
              type="submit"
              disabled={loading}
              className={
                loading ? styles.modalDisabledButton : styles.modalSaveButton
              }
            >
              {productData?.modalType === 'ADD'
                ? 'Agregar producto'
                : 'Guardar cambios'}
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default ItemsCreateModal
