import axios from 'axios'
import { useState, useEffect } from 'react'
import { Form, Image, Modal } from 'react-bootstrap'
import { toast } from 'react-toastify'
import styles from '../index.module.css'

const BrandsCreateModal = ({
  show,
  handleClose,
  getAllIndusries,
  brandData,
}) => {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [image, setImage] = useState([])
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    if (show) setName(brandData?.industry.name || '')
    setDescription(brandData?.industry.description || '')
  }, [show])
  console.log('dwfdf', brandData)
  const addItem = (e) => {
    e.preventDefault()
    setLoading(true)
    axios({
      method: 'POST',
      url: `/brands`,
      data: {
        name,
        ...(description && { description }),
        // image: image[0].link,
        published: true,
      },
    })
      .then((res) => {
        closeStateAndClearModal()
        getAllIndusries()
        setLoading(false)
        toast('Marca agregada!')
      })
      .catch((err) => {
        setLoading(false)
        toast('Error al agregar la marca')
      })
  }
  const editItem = (e) => {
    e.preventDefault()
    setLoading(true)
    axios({
      method: 'PUT',
      url: `/brands/${brandData.industry.id}`,
      data: {
        name,
        ...(description && { description }),

        published: true,
      },
    })
      .then((res) => {
        closeStateAndClearModal()
        getAllIndusries()
        setLoading(false)
        toast('Marca editada!')
      })
      .catch((err) => {
        setLoading(false)
        toast('Error al editar la marca')
      })
  }
  const submitHandler = (e) => {
    if (brandData.modalType === 'ADD') {
      addItem(e)
    } else editItem(e)
  }
  const closeStateAndClearModal = () => {
    handleClose()
    setName('')
    setDescription('')
    setImage([])
  }
  console.log('img', image)
  return (
    <Modal
      show={show}
      onHide={closeStateAndClearModal}
      fullscreen="md-down"
      backdrop="static"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div
          className={styles.mobileModalDiv}
          onClick={() => closeStateAndClearModal()}
        >
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            {brandData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Marca
          </p>
        </div>
        <button
          type="button"
          onClick={() => closeStateAndClearModal()}
          className={styles.modalCloseButton}
        >
          <Image src="/images/close.png" alt="X" />
        </button>
        <h3 className={styles.modalHeadingMain}>
          {' '}
          {brandData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Marca
        </h3>
        <Form onSubmit={(e) => submitHandler(e)}>
          <Form.Label className={styles.modalLabel}>
            Nombre de la marca <span style={{ color: '#fc5447' }}>*</span>
          </Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Brand name"
            onChange={(e) => setName(e.target.value)}
            value={name}
            required
            className={styles.modalInput}
          />
          <Form.Label className={styles.modalLabel}>Descripción</Form.Label>
          <Form.Control
            placeholder="Enter description"
            onChange={(e) => setDescription(e.target.value)}
            value={description}
            className={styles.modalInput}
            as="textarea"
          />
          <div className={styles.modalButtonDiv}>
            <button
              type="submit"
              disabled={loading || name === ''}
              className={
                loading || name === ''
                  ? styles.modalDisabledButton
                  : styles.modalSaveButton
              }
            >
              {brandData?.modalType === 'ADD' ? 'Agregar' : 'Editar'}
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default BrandsCreateModal
