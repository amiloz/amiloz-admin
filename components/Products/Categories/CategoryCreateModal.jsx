import axios from 'axios'
import { useState, useEffect } from 'react'
import { Form, Image, Modal } from 'react-bootstrap'
import styles from '../index.module.css'
import Dropzone from '../../UI/Dropzone'
import { toast } from 'react-toastify'

const CategoryCreateModal = ({
  show,
  handleClose,
  getAllIndusries,
  categoryData,
}) => {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [image, setImage] = useState([])
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    if (show) setName(categoryData?.category.name || '')
    setDescription(categoryData?.category.description || '')
  }, [show])
  const addItem = () => {
    setLoading(true)
    axios({
      method: 'POST',
      url: `/categories`,
      data: {
        name,
        ...(description && { description }),
        // image: image[0].link,
        published: true,
      },
    })
      .then((res) => {
        closeStateAndClearModal()
        getAllIndusries()
        setLoading(false)
        toast('Category added!')
      })
      .catch((err) => {
        setLoading(false)
        toast('Error adding category')
      })
  }
  const editItem = () => {
    setLoading(true)
    axios({
      method: 'PUT',
      url: `/categories/${categoryData.category.id}`,
      data: {
        name,
        ...(description && { description }),
        published: true,
      },
    })
      .then((res) => {
        closeStateAndClearModal()
        getAllIndusries()
        setLoading(false)
        toast('Category edited!')
      })
      .catch((err) => {
        setLoading(false)
        toast('Error adding category')
      })
  }
  const submitHandler = (e) => {
    e.preventDefault()
    if (categoryData.modalType === 'ADD') addItem()
    else editItem()
  }

  const closeStateAndClearModal = () => {
    handleClose()
    setName('')
    setDescription('')
    setImage([])
  }
  console.log('img', image)
  return (
    <Modal
      show={show}
      onHide={closeStateAndClearModal}
      fullscreen="md-down"
      backdrop="static"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div
          className={styles.mobileModalDiv}
          onClick={() => closeStateAndClearModal()}
        >
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            {categoryData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Categoria
          </p>
        </div>
        <button
          type="button"
          onClick={() => closeStateAndClearModal()}
          className={styles.modalCloseButton}
        >
          <Image src="/images/close.png" alt="X" />
        </button>
        <h3 className={styles.modalHeadingMain}>
          {' '}
          {categoryData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Categoria
        </h3>
        <Form onSubmit={(e) => submitHandler(e)}>
          <Form.Label className={styles.modalLabel}>
            Nombre de la Categoria <span style={{ color: '#fc5447' }}>*</span>
          </Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Category name"
            onChange={(e) => setName(e.target.value)}
            value={name}
            required
            className={styles.modalInput}
          />
          <Form.Label className={styles.modalLabel}>Descripción</Form.Label>
          <Form.Control
            placeholder="Enter description"
            onChange={(e) => setDescription(e.target.value)}
            value={description}
            className={styles.modalInput}
            as="textarea"
          />
          <div className={styles.modalButtonDiv}>
            <button
              type="submit"
              disabled={loading || name === ''}
              className={
                loading || name === ''
                  ? styles.modalDisabledButton
                  : styles.modalSaveButton
              }
            >
              {categoryData?.modalType === 'ADD' ? 'Agregar' : 'Editar'}
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default CategoryCreateModal
