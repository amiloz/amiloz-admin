import { useState } from 'react'
import styles from './index.module.css'
import Button from '../UI/Button'

import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai'
import { useLogin } from '../../hooks/api-calls/auth/login'

const Login = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  // hooks
  const { isLoading: loggingIn, mutate: login } = useLogin()

  const submitFn = () => {
    login({
      email: email,
      password: password,
      source: 'admin_panel',
    })
  }

  return (
    <div className={styles.LoginRegister}>
      <div className={styles.Container}>
        <p className={styles.Heading}>Iniciar sesión</p>
        <p className={styles.SubHeading}>Ingrea a tu cuenta con Amiloz</p>
        <InputField
          title="Correo"
          type="text"
          value={email}
          setValue={setEmail}
          placeholder="Ingresa tu correo"
        />
        <InputField
          title="Contraseña"
          type="password"
          id="password"
          value={password}
          setValue={setPassword}
          placeholder="Ingresa tu contraseña"
        />
        <Button
          onClick={submitFn}
          className={styles.SubmitButton}
          disabled={loggingIn}
          block
        >
          Iniciar sesión {loggingIn ? '...' : null}
        </Button>
      </div>
    </div>
  )
}

export default Login

const InputField = (props) => {
  const { title, type, value, setValue, placeholder, id } = props
  const [changedType, setChangedType] = useState(type)
  return (
    <div className={styles.InputField}>
      <div className={styles.InputTitle}>{title}</div>
      <input
        className={styles.InputBox}
        placeholder={placeholder}
        type={type === 'password' ? changedType : type}
        value={value}
        id={id}
        onChange={(e) => setValue(e.target.value)}
      />
      {/* for password id show eye icon */}
      {id === 'password' ? (
        changedType === 'password' ? (
          <AiFillEyeInvisible
            className={styles.Eye}
            onClick={() => {
              setChangedType('text')
            }}
          />
        ) : (
          <AiFillEye
            className={styles.Eye}
            onClick={() => {
              setChangedType('password')
            }}
          />
        )
      ) : null}
    </div>
  )
}
