import styles from '../Orders/index.module.css'
import stylesDash from './index.module.css'
import { useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap'
import moment from 'moment'
import axios from 'axios'
import { useRouter } from 'next/dist/client/router'
import withAuth from '../../hoc/withAuth'
import DatePicker from 'react-datepicker'

const Dashboard = () => {
  const [orderStats, setOrderStats] = useState({
    amount: 0,
    orders: 0,
    products: 0,
  })
  const [date, setDate] = useState(new Date())
  const [deliveryRep, setDeliveryRep] = useState('')
  const [deliveryRepList, setDeliveryRepList] = useState([])
  const [windowWidth, setWindowWidth] = useState(0)
  const { name } = useSelector((state) => state.auth)
  const router = useRouter()
  useEffect(() => {
    setWindowWidth(window.innerWidth)
  }, [])
  const getStats = (dateFilter = new Date(), deliveryRepFilter) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/dashboard/orders/stats?date=${moment(dateFilter).format(
      'YYYY-MM-DD'
    )}`
    if (deliveryRepFilter) {
      url += `&delivery_rep_id=${deliveryRepFilter}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setOrderStats(res.data.order_stats)
    })
  }
  const getDeliveryReps = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/users/all?role=delivery_rep`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setDeliveryRepList(res.data.users)
    })
  }
  // const getAllIndusries = () => {
  //   const token = JSON.parse(localStorage.getItem('token'))
  //   let url = `/orders?_limit=30&_page=1&date=${moment().format('YYYY-MM-DD')}`
  //   axios({
  //     method: 'GET',
  //     url,
  //     headers: {
  //       Authorization: 'Bearer ' + token.access_token,
  //     },
  //   }).then((res) => {
  //     setIndustries(res.data.orders)
  //   })
  // }
  // const getSummary = () => {
  //   const token = JSON.parse(localStorage.getItem('token'))
  //   axios({
  //     method: 'POST',
  //     url: `/orders/summary`,
  //     headers: {
  //       Authorization: 'Bearer ' + token.access_token,
  //     },
  //     data: {
  //       date: moment().format('YYYY-MM-DD'),
  //     },
  //   }).then((res) => {
  //     setSummary(res.data.products)
  //   })
  // }
  useEffect(() => {
    getStats()
    getDeliveryReps()
  }, [])
  return (
    <div className={stylesDash.Container}>
      <h2 className={styles.orderPlaceDivHeading}>Bienvenido {name}</h2>
      <Row>
        <div style={{ display: 'flex' }}>
          <div>
            <label className={styles.modalLabel}>Filtrar por fecha</label>
            <DatePicker
              selected={date}
              onChange={(date) => {
                setDate(date)
                getStats(date, deliveryRep)
              }}
              placeholderText="Filtrar por fecha"
              className={styles.modalInputDate}
            />
          </div>
          <div style={{ marginLeft: '20px' }}>
            <label className={styles.modalLabel}>Filtrar por repartidor</label>
            <select
              value={deliveryRep}
              onChange={(e) => {
                setDeliveryRep(e.target.value)
                getStats(date, e.target.value)
              }}
              className={styles.modalInput}
              style={{ padding: '4.5px 16px' }}
            >
              <option hidden>All</option>
              <option selected value="">
                All
              </option>
              {deliveryRepList?.map((rep) => (
                <option key={rep.id} value={rep.id}>
                  {rep.name}
                </option>
              ))}
            </select>
          </div>
        </div>
      </Row>
      <Row style={{ margin: '20px 0' }}>
        <Col
          lg={4}
          md={12}
          style={windowWidth > 1000 ? { paddingLeft: '0' } : {}}
        >
          <div className={styles.orderPlaceDiv}>
            <h3 className={styles.detailPageHeadingSub}>Pedidos Totales</h3>
            {orderStats.orders}
          </div>
        </Col>
        <Col lg={4} md={12}>
          <div className={styles.orderPlaceDiv}>
            <h3 className={styles.detailPageHeadingSub}>Productos Totales</h3>
            {orderStats.products}
          </div>
        </Col>
        <Col lg={4} md={12}>
          <div className={styles.orderPlaceDiv}>
            <h3 className={styles.detailPageHeadingSub}>Pesos Totales</h3>$
            {orderStats.amount}
          </div>
        </Col>
      </Row>
      {/* <Row style={{ margin: '20px 0' }}>
        <Col lg={6} md={12}>
          <div className={styles.orderPlaceDiv}>
            <h3 className={styles.detailPageHeadingSub}>Today Orders</h3>
            <div
              style={{
                height: '64vh',
                overflowY: 'auto',
                overflowX: 'hidden',
                paddingRight: '7px',
              }}
            >
              {industries.length === 0 ? (
                <span className={styles.OrderName}>No orders today</span>
              ) : (
                <table className={styles.myTable}>
                  <thead>
                    <tr>
                      <th className={styles.theadTh}>Store</th>
                      <th className={styles.theadTh1}>Grand Total</th>
                      <th className={styles.theadTh1}>Status</th>
                      <th className={styles.theadTh1}>View Details</th>
                    </tr>
                  </thead>
                  <tbody>
                    {industries?.map((industry) => {
                      const { id, grand_total, status, store } = industry
                      return (
                        <tr
                          key={id}
                          className={styles.tbodyTr}
                          style={{ margin: '12px 0' }}
                        >
                          <td className={styles.tbodyTd}>
                            <div>
                              <span className={styles.OrderName}>
                                {store.name}
                              </span>
                              <span className={styles.OrderContact}>
                                {store.mobile}
                              </span>
                              <br />
                              <span className={styles.OrderName}>
                                {store.poc_name}
                              </span>
                              <span className={styles.OrderContact}>
                                {store.poc_mobile}
                              </span>
                              <br />
                              <span className={styles.OrderContact}>
                                {store.street}, {store.city}, {store.state},{' '}
                                {store.country}
                              </span>
                            </div>
                          </td>
                          <td className={styles.tbodyTd1}>{grand_total}</td>
                          <td className={styles.tbodyTd1}>{status}</td>
                          <td className={styles.tbodyTd1}>
                            <button
                              onClick={() => {
                                router.push(`/orders/${id}`)
                              }}
                              className={styles.buttonEditProd}
                            >
                              View Details
                            </button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              )}
            </div>
          </div>
        </Col>
        <Col lg={6} md={12}>
          <div className={styles.orderPlaceDiv}>
            <h3 className={styles.detailPageHeadingSub}>
              Today Orders Summary
            </h3>
            <div
              style={{
                height: '64vh',
                overflowY: 'auto',
                overflowX: 'hidden',
                paddingRight: '7px',
              }}
            >
              <Row>
                <Col lg={2} className={styles.orderSummaryHeader}>
                  Image
                </Col>
                <Col lg={5} className={styles.orderSummaryHeader}>
                  Name
                </Col>
                <Col lg={3} className={styles.orderSummaryHeader}>
                  Specifications
                </Col>
                <Col lg={2} className={styles.orderSummaryHeader}>
                  <div style={{ float: 'right' }}>Leftovers</div>
                </Col>
              </Row>
              {summary?.map((prod) =>
                summary.length === 0 ? (
                  <span className={styles.OrderName}>No orders today</span>
                ) : (
                  <div className={styles.summaryDiv} key={prod.product_id}>
                    <Row>
                      <Col lg={2}>
                        <Image
                          src={prod.primary_image}
                          alt=""
                          width="50px"
                          height="50px"
                        />
                      </Col>
                      <Col lg={5} style={{ paddingTop: '12px' }}>
                        <h2 className={styles.summaryHeading}>
                          {prod.product_name}
                        </h2>
                      </Col>
                      <Col lg={3} style={{ paddingTop: '12px' }}>
                        <p className={styles.summaryDesc}>
                          <span
                            style={{
                              fontSize: '12px',
                              lineHeight: '16px',
                              color: '#0F1111',
                              fontWeight: '500',
                            }}
                          >
                            Quantity:
                          </span>{' '}
                          {prod.quantity}
                          <br />
                          <span
                            style={{
                              fontSize: '12px',
                              lineHeight: '16px',
                              color: '#0F1111',
                              fontWeight: '500',
                            }}
                          >
                            Size:
                          </span>{' '}
                          {prod.size}
                          <br />
                          <span
                            style={{
                              fontSize: '12px',
                              lineHeight: '16px',
                              color: '#0F1111',
                              fontWeight: '500',
                            }}
                          >
                            Unit in package:
                          </span>{' '}
                          {prod.unit_in_package}
                        </p>
                      </Col>
                      <Col lg={2} style={{ paddingTop: '12px' }}>
                        <p
                          className={styles.summaryDesc}
                          style={{ float: 'right' }}
                        >
                          {prod.left_quantity}
                        </p>
                      </Col>
                    </Row>
                  </div>
                )
              )}
            </div>
          </div>
        </Col>
      </Row> */}
    </div>
  )
}

export default withAuth(Dashboard)
