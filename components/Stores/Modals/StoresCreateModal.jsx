import axios from 'axios'
import { useState, useEffect } from 'react'
import {
  Col,
  Form,
  Image,
  Modal,
  OverlayTrigger,
  Row,
  Tooltip,
} from 'react-bootstrap'
import styles from '../index.module.css'
import Dropzone from '../../UI/Dropzone'

const StoresCreateModal = ({
  show,
  handleClose,
  getAllIndusries,
  storeData,
}) => {
  const [name, setName] = useState('')
  const [mobile, setMobile] = useState('')
  const [pocName, setPocName] = useState('')
  const [pocMobile, setPocMobile] = useState('')
  const [description, setDescription] = useState('')
  const [street, setStreet] = useState('')
  const [city, setCity] = useState('')
  const [state, setState] = useState('')
  const [country, setCountry] = useState('')
  const [postalCode, setPostalCode] = useState('')
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    if (show) setName(storeData.store?.name || '')
    setMobile(storeData.store?.mobile || '')
    setPocName(storeData.store?.poc_name || '')
    setPocMobile(storeData.store?.poc_mobile || '')
    setDescription(storeData.store?.description || '')
    setStreet(storeData.store?.street || '')
    setCity(storeData.store?.city || '')
    setState(storeData.store?.state || '')
    setCountry(storeData.store?.country || '')
    setPostalCode(storeData.store?.postal_code || '')
  }, [show])
  const addItem = (e) => {
    e.preventDefault(e)
    if (mobile.toString().length !== 10) {
      toast('Invalid mobile number')
      setLoading(false)
      return false
    }
    if (
      pocMobile !== null &&
      pocMobile !== '' &&
      pocMobile.toString().length !== 10
    ) {
      toast('Invalid person mobile number')
      setLoading(false)
      return false
    }
    setLoading(true)
    axios({
      method: 'POST',
      url: `/stores`,
      data: {
        name,
        mobile,
        poc_name: pocName === '' ? null : pocName,
        poc_mobile: pocMobile === '' ? null : pocMobile.toString(),
        ...(description && { description }),
        street,
        city,
        state,
        country,
        postal_code: postalCode,
      },
    })
      .then((res) => {
        closeModalAndClearState()
        getAllIndusries()
        setLoading(false)
      })
      .catch((err) => {
        setLoading(false)
      })
  }
  const editItem = (e) => {
    e.preventDefault(e)
    if (mobile.toString().length !== 10) {
      toast('Invalid mobile number')
      setLoading(false)
      return false
    }
    if (
      pocMobile !== null &&
      pocMobile !== '' &&
      pocMobile.toString().length !== 10
    ) {
      toast('Invalid person mobile number')
      setLoading(false)
      return false
    }
    setLoading(true)
    axios({
      method: 'PUT',
      url: `/stores/${storeData?.store.id}`,
      data: {
        name,
        mobile: mobile.toString(),
        poc_name: pocName === '' ? null : pocName,
        poc_mobile: pocMobile === '' ? null : pocMobile.toString(),
        ...(description && { description }),
        street,
        city,
        state,
        country,
        postal_code: postalCode,
      },
    })
      .then((res) => {
        closeModalAndClearState()
        getAllIndusries()
        setLoading(false)
      })
      .catch((err) => {
        setLoading(false)
      })
  }
  const submitHandler = (e) => {
    if (storeData?.modalType === 'ADD') {
      addItem(e)
    } else {
      editItem(e)
    }
  }
  const closeModalAndClearState = () => {
    handleClose()
    setName('')
    setMobile('')
    setPocName('')
    setPocMobile('')
    setDescription('')
    setStreet('')
    setCity('')
    setState('')
    setCountry('')
    setPostalCode('')
  }
  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      10 digit mobile number without + and dashes
    </Tooltip>
  )
  return (
    <Modal
      show={show}
      onHide={closeModalAndClearState}
      fullscreen="md-down"
      backdrop="static"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div
          className={styles.mobileModalDiv}
          onClick={() => closeModalAndClearState()}
        >
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            {storeData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Tienda
          </p>
        </div>
        <button
          type="button"
          onClick={() => closeModalAndClearState()}
          className={styles.modalCloseButton}
        >
          <Image src="/images/close.png" alt="X" />
        </button>
        <h3 className={styles.modalHeadingMain}>
          {' '}
          {storeData?.modalType === 'ADD' ? 'Agregar' : 'Editar'} Tienda
        </h3>
        <Form onSubmit={(e) => submitHandler(e)}>
          <div
            style={{
              height: '60vh',
              overflowY: 'auto',
              paddingRight: '5px',
              overflowX: 'hidden',
            }}
          >
            <Form.Label className={styles.modalLabel}>
              Nombre de la tienda <span style={{ color: '#fc5447' }}>*</span>
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter store name"
              onChange={(e) => setName(e.target.value)}
              value={name}
              required
              className={styles.modalInput}
            />
            <Form.Label className={styles.modalLabel}>
              Contacto de la tienda <span style={{ color: '#fc5447' }}>*</span>
              <OverlayTrigger
                placement="right"
                delay={{ show: 250, hide: 400 }}
                overlay={renderTooltip}
              >
                <span
                  style={{
                    background: '#fc5447',
                    padding: '0 5px',
                    borderRadius: '50%',
                    marginLeft: '20px',
                    color: 'white',
                  }}
                >
                  ?
                </span>
              </OverlayTrigger>
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter store contact number"
              onChange={(e) => setMobile(e.target.value)}
              value={mobile}
              required
              className={styles.modalInput}
            />
            <Form.Label className={styles.modalLabel}>
              Nombre de la persona
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter name"
              onChange={(e) => setPocName(e.target.value)}
              value={pocName}
              className={styles.modalInput}
            />
            <Form.Label className={styles.modalLabel}>
              Persona de contacto{' '}
              <OverlayTrigger
                placement="right"
                delay={{ show: 250, hide: 400 }}
                overlay={renderTooltip}
              >
                <span
                  style={{
                    background: '#fc5447',
                    padding: '0 5px',
                    borderRadius: '50%',
                    marginLeft: '20px',
                    color: 'white',
                  }}
                >
                  ?
                </span>
              </OverlayTrigger>
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter contact number"
              onChange={(e) => setPocMobile(e.target.value)}
              value={pocMobile}
              className={styles.modalInput}
            />
            <Row>
              <Col>
                <Form.Label className={styles.modalLabel}>
                  Calle <span style={{ color: '#fc5447' }}>*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter street"
                  onChange={(e) => setStreet(e.target.value)}
                  value={street}
                  required
                  className={styles.modalInput}
                />
              </Col>
              <Col>
                <Form.Label className={styles.modalLabel}>
                  Ciudad <span style={{ color: '#fc5447' }}>*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter city"
                  onChange={(e) => setCity(e.target.value)}
                  value={city}
                  required
                  className={styles.modalInput}
                />
              </Col>
              <Col>
                <Form.Label className={styles.modalLabel}>
                  Estado <span style={{ color: '#fc5447' }}>*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter state"
                  onChange={(e) => setState(e.target.value)}
                  value={state}
                  required
                  className={styles.modalInput}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Label className={styles.modalLabel}>
                  País <span style={{ color: '#fc5447' }}>*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter country"
                  onChange={(e) => setCountry(e.target.value)}
                  value={country}
                  required
                  className={styles.modalInput}
                />
              </Col>
              <Col>
                <Form.Label className={styles.modalLabel}>
                  Código postal <span style={{ color: '#fc5447' }}>*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter postal code"
                  onChange={(e) => setPostalCode(e.target.value)}
                  value={postalCode}
                  required
                  className={styles.modalInput}
                />
              </Col>
            </Row>
            <Form.Label className={styles.modalLabel}>Descripción</Form.Label>
            <Form.Control
              placeholder="Enter description"
              onChange={(e) => setDescription(e.target.value)}
              value={description}
              className={styles.modalInput}
              as="textarea"
            />
          </div>
          <div className={styles.modalButtonDiv}>
            <button
              type="submit"
              disabled={loading || name === ''}
              className={
                loading || name === ''
                  ? styles.modalDisabledButton
                  : styles.modalSaveButton
              }
            >
              {storeData?.modalType === 'ADD' ? 'Agregar' : 'Editar'}
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default StoresCreateModal
