import React from 'react'
import withAuth from '../../hoc/withAuth'
import styles from './index.module.css'
import StoresTable from './StoresTable'

function Stores() {
  return (
    <div className={styles.Container}>
      <StoresTable />
    </div>
  )
}

export default withAuth(Stores)
