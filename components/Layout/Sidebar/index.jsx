import Image from 'next/image'
import styles from './index.module.css'
import { IoMdArrowDropdown } from 'react-icons/io'
import Backdrop from '../../UI/Backdrop'
import { useRouter } from 'next/dist/client/router'

const items = [
  {
    icon: <i className="fa fa-tachometer" aria-hidden="true"></i>,
    title: 'Dashboard',
    url: '/dashboard',
  },
  {
    icon: <i className="fa fa-shopping-cart" aria-hidden="true"></i>,
    title: 'Productos',
    url: '/products',
    subcategory: [
      { title: 'Inventario', query: 'items' },
      { title: 'Marcas', query: 'brands' },
      { title: 'Categorias', query: 'categories' },
    ],
  },
  {
    icon: <i className="fa fa-archive" aria-hidden="true"></i>,
    title: 'Tiendas',
    url: '/stores',
  },
  {
    icon: <i className="fa fa-users" aria-hidden="true"></i>,
    title: 'Usuarios',
    url: '/user',
    subcategory: [
      { title: 'Vendedor', query: 'sales-rep' },
      { title: 'Repartidor', query: 'delivery-rep' },
    ],
  },
  {
    icon: <i className="fa fa-handshake-o" aria-hidden="true"></i>,
    title: 'Pedidos',
    url: '/orders',
    subcategory: [
      { title: 'Pedidos', query: 'orders' },
      { title: 'Productos sin vender', query: 'left-over' },
    ],
  },
]

const Sidebar = (props) => {
  const { sidebarOpen, setSidebarOpen, setShowSettingDropdown } = props
  const router = useRouter()
  const url = router.pathname
  const tab = router.query?.tab

  return (
    <>
      <div
        className={`${styles.Sidebar} ${
          sidebarOpen ? styles.SidebarOpen : styles.SidebarClose
        }`}
        onClick={() => setShowSettingDropdown(false)}
      >
        <div className={`text-center ${styles.LogoContainer}`}>
          <Image
            width="170px"
            height="35px"
            className="cursor-pointer "
            src="/images/amiloz-logo.svg"
            alt="Amiloz"
            unoptimized={true}
            onClick={() => router.push('/')}
          />
        </div>
        <div className="pt-5">
          {items.map((item, idx) => {
            return (
              <details
                key={idx}
                className={styles.DetailItem}
                onClick={() => {
                  if (url !== item.url && !item?.subcategory?.length) {
                    router.push(item.url)
                    setSidebarOpen(false)
                  }
                }}
              >
                <summary
                  className={
                    url === '/' && item.url === '/dashboard'
                      ? `${styles.Item} ${styles.ActiveItem}`
                      : `${styles.Item} ${
                          url === item.url ? styles.ActiveItem : ''
                        }`
                  }
                >
                  <span>
                    {item.icon}&nbsp;&nbsp;&nbsp;{item.title}
                  </span>
                  {item.subcategory ? (
                    <span className={styles.DropdownIcon}>
                      <IoMdArrowDropdown />
                    </span>
                  ) : null}
                </summary>
                {item.subcategory ? (
                  <div>
                    {item.subcategory.map((subitem, idx) => (
                      <div
                        key={idx}
                        className={`${styles.Subitem} ${
                          tab === subitem.query ? styles.ActiveSubitem : ''
                        }`}
                        onClick={() => {
                          router.push(
                            `${item.url}?tab=${subitem.query}`,
                            undefined,
                            {
                              shallow: true,
                            }
                          )
                          setSidebarOpen(false)
                        }}
                      >
                        {subitem.title}
                      </div>
                    ))}
                  </div>
                ) : null}
              </details>
            )
          })}
        </div>
      </div>
      {sidebarOpen ? <Backdrop clicked={() => setSidebarOpen(false)} /> : null}
    </>
  )
}

export default Sidebar
