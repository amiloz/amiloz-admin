import Head from 'next/head'
import ProductsRoute from '../../components/Products'

const StoresPage = () => {
  return (
    <>
      <Head>
        <title>Products</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.jpeg" />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        />
      </Head>
      <ProductsRoute />
    </>
  )
}

export default StoresPage
